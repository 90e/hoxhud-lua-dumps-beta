These dumps are taken from HoxHud P2.2


./luac/ contains the raw dumped compiled Lua.

./lua/ contains the decompiled Lua.

./standalone/ contains files to use HoxHud without its Windows binaries (IPHLPAPI/PD2API1)


All files use UNIX style line breaks (\n)


Credits:

-90e    - Reverse engineered and dumped the Lua

-gir489 - Defeating TheMida
