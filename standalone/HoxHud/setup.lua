-- 90e
-- This is compatible with P3.1/1.1852010072PR only
if RequiredScript then
	local requires = 
	{
		["core/lib/managers/coremusicmanager"] = "CoreMusicManagerV2.luac",
		["lib/managers/chatmanager"] = "ChatManagerV2.luac",
		["lib/managers/hudmanagerpd2"] = {"HUDEnemyTarget.luac", "HUDTimerManager.luac", "HUDManagerV2.luac"},
		["lib/managers/localizationmanager"] = "LocalizationManagerV2.luac",
		["lib/managers/lootmanager"] = "LootManagerV2.luac",
		["lib/managers/menumanager"] = "MenuManagerV2.luac",
		["lib/managers/missionassetsmanager"] = "MissionAssetsManagerV2.luac",
		["lib/managers/playermanager"] = "PlayerManagerV2.luac",
		["lib/utils/game_state_machine/gamestatemachine"] = "AntiCheat.luac",
		["lib/managers/skilltreemanager"] = "SkillTreeManagerV2.luac",
		["lib/managers/statisticsmanager"] = "StatisticsManagerV2.luac",
		["lib/managers/group_ai_states/groupaistatebase"] = "GroupAIStateBaseV2.luac",
		["lib/managers/hud/hudassaultcorner"] = "HUDAssaultCornerV2.luac",
		["lib/managers/hud/hudhitdirection"] = "HUDHitDirectionV2.luac",
		["lib/managers/hud/hudinteraction"] = "HUDInteractionV2.luac",
		["lib/managers/hud/hudplayercustody"] = "HUDPlayerCustodyV2.luac",
		["lib/managers/hud/hudstatsscreen"] = "HUDStatsScreenV2.luac",
		["lib/managers/hud/hudsuspicion"] = "HUDSuspicionV2.luac",
		["lib/managers/hud/hudteammate"] = "HUDTeammateV2.luac",
		["lib/managers/menu/missionbriefinggui"] = "MissionBriefingGuiV2.luac",
		["lib/network/base/basenetworksession"] = "BaseNetworkSessionV2.luac",
		["lib/network/base/clientnetworksession"] = "ClientNetworkSessionV2.luac",
		["lib/network/base/hostnetworksession"] = "HostNetworkSessionV2.luac",
		["lib/network/extensions/cop/huskcopdamage"] = "HuskCopDamageV2.luac",
		["lib/network/handlers/unitnetworkhandler"] = "UnitNetworkHandlerV2.luac",
		["lib/states/ingamewaitingforplayers"] = "IngameWaitingForPlayersV2.luac",
		["lib/states/missionendstate"] = "MissionEndStateV2.luac",
		["lib/tweak_data/tweakdata"] = "TweakDataV2.luac",
		["lib/units/beings/player/playerdamage"] = "PlayerDamageV2.luac",
		["lib/units/beings/player/playerequipment"] = "PlayerEquipmentV2.luac",
		["lib/units/beings/player/states/playerstandard"] = "PlayerStandardV2.luac",
		["lib/units/cameras/fpcameraplayerbase"] = "FPCameraPlayerBaseV2.luac",
		["lib/units/equipment/ecm_jammer/ecmjammerbase"] = "ECMJammerBaseV2.luac",
		["lib/units/equipment/sentry_gun/sentrygundamage"] = "SentryGunDamageV2.luac",
		["lib/units/interactions/interactionext"] = "InteractionExtV2.luac",
		["lib/units/props/digitalgui"] = "DigitalGuiV2.luac",
		["lib/units/props/securitycamera"] = "SecurityCameraV2.luac",
		["lib/units/props/timergui"] = "TimerGuiV2.luac",
		["lib/units/weapons/sentrygunweapon"] = "SentryGunWeaponV2.luac",
		["lib/units/weapons/weaponlaser"] = "WeaponLaserV2.luac",
		["lib/units/enemies/cop/copdamage"] = {"HUDCopDamage.luac", "CopDamageV2.luac"},
		["lib/units/props/texttemplatebase"] = "TextTemplateBaseV2.luac"
	}
	
	-- Hijack the Application metatable and give it HoxHud functions
	local metatable = getmetatable(Application)
	if not metatable.__set_a2a then
		-- IdToColor is meer sanity checking. The result doesn't matter
		local old_index = metatable.__index
		local added_to_application = {IdToColor = function() return true end, ColorToId = function() end, ColAlloc = function() end}
		
		local function Application__index(t, k)
			if added_to_application[k] then
				return added_to_application[k]
			end
			
			return old_index[k]
		end
		
		metatable.__index = Application__index
		metatable.__set_a2a = true
	end
	
	-- Make sure everything exists
	-- BUG #1: tweak_data is sometimes nil, but it will exist at least once during a require
	if tweak_data and not tweak_data.hoxhud then
		dofile("HoxHud/HoxHudTweakData.lua")
		tweak_data.hoxhud = HoxHudTweakData:new()
	end
	
	if not _G.HoxHudVer then
		_G.HoxHudVer = "1.1852010072PR" 
	end
	
	if not _G.clone_methods then
		-- This isn't what came with HoxHud, I had to change it to fix a stack overflow glitch
		function _G.clone_methods(ThisClass)
			if not ThisClass.oldMethods then 
				ThisClass.oldMethods = clone(ThisClass) 
			end
		end
	end
	
	if not _G.wrap_methods then
		function _G.wrap_methods(ThisClass) 
			for k,v in pairs(ThisClass) do 
				if type(v) == "function" then 
					ThisClass[k] = function(...) 
						local ret = { pcall(v, ...) } 
						if ret[1] and #ret > 1 then 
							table.remove(ret, 1) 
							return unpack(ret) 
						end 
					end 
				end 
			end 
		end 
	end
	
	local _RequiredScript = RequiredScript:lower()
	-- Fixes a bug related to crashing when playing MP and a cop dies
	if _RequiredScript == "lib/managers/gameplaycentralmanager" then
		local old_spawn_pickup = GamePlayCentralManager.spawn_pickup
		function GamePlayCentralManager:spawn_pickup(params)
			if not Network:is_client() then
				old_spawn_pickup(self, params)
			end
		end
			
		return
	end
		
	if requires[_RequiredScript] then
		local base_path = "HoxHud/Binaries/"
		
		-- Fixes a bug related to clone_methods
		if _RequiredScript == "lib/network/base/clientnetworksession" then
			ClientNetworkSession.oldMethods.send_to_host = ClientNetworkSession.oldMethods.send_to_host or ClientNetworkSession.send_to_host
		end
		
		if type(requires[_RequiredScript]) == "table" then
			for k, v in pairs(requires[_RequiredScript]) do
				dofile(base_path .. v)
			end
		elseif type(requires[_RequiredScript]) == "string" then
			dofile(base_path .. requires[_RequiredScript])
		end
		
		if _RequiredScript == "lib/managers/hudmanagerpd2" then
			-- Apparently HoxHud developers are trying to stop me from making these dumps by moving minor functions to C.
			function HUDManager:_count_dominated_cops(enemies)
				--return managers.groupai:state():police_hostage_count() -- According to gir489 this won't work as client. Do me a favor and tell him hi smom doesn't work as client, LOLL!!!!

				local count = 0
				for _, cop in pairs(managers.enemy:all_enemies()) do
					if cop.unit:anim_data() and cop.unit:anim_data().hands_tied then
						count = count + 1
					end
				end

				return count
			end

			function HUDManager:_count_gage_packages(interaction_objects)
				local nr = 0
				
				for _, unit in pairs(interaction_objects) do
					for _, assignment_data in pairs(tweak_data.gage_assignment.assignments) do
						if assignment_data.unit == unit:name() then
							nr = nr + 1
						end
					end
				end
				
				return nr
			end
			
			function HUDManager:change_waypoint_arrow_color() end
			function HUDManager:change_waypoint_icon_color() end
			function HUDManager:change_waypoint_icon() end
		end
	end
end
