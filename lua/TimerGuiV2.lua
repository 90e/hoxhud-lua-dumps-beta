clone_methods(TimerGui)
function TimerGui._create_hud_timer(_ARG_0_)
	if _ARG_0_._hud_timer then
		return
	end

	_ARG_0_._in_tab_screen = false
	if _ARG_0_._unit and _ARG_0_._unit.interaction then
		_ARG_0_._in_tab_screen = tweak_data.hoxhud.tab_screen_timers[_ARG_0_._unit:interaction().tweak_data]
	end

	_ARG_0_._hud_timer = managers.hud:add_hud_timer(_ARG_0_._in_tab_screen, tweak_data.hoxhud.timer_name_map[_ARG_0_._unit:interaction().tweak_data] or _ARG_0_._unit:interaction().tweak_data or " ", {
		tweak_name = _ARG_0_._unit:interaction().tweak_data,
		complete_color = tweak_data.hoxhud.timer_complete_color,
		text_color = tweak_data.hoxhud.timer_text_color,
		timer_flash = tweak_data.hoxhud.timer_broken_flash,
		text_color = tweak_data.hoxhud.timer_text_color,
		text_flash = tweak_data.hoxhud.timer_broken_flash,
		flash_period = tweak_data.hoxhud.timer_flash_speed
	})
	if _ARG_0_._hud_timer then
		_ARG_0_._hud_timer:set_visible(false)
	end
end

function TimerGui._start(_ARG_0_, ...)
	_ARG_0_:_create_hud_timer()
	if _ARG_0_._hud_timer then
		_ARG_0_._hud_timer:set_visible(true)
	end

	_ARG_0_.oldMethods._start(_ARG_0_, ...)
end

function TimerGui.done(_ARG_0_)
	_ARG_0_.oldMethods.done(_ARG_0_)
	if _ARG_0_._hud_timer then
		managers.hud:del_hud_timer(_ARG_0_._in_tab_screen, _ARG_0_._hud_timer)
		_ARG_0_._hud_timer = nil
	end
end

function TimerGui.destroy(_ARG_0_, ...)
	_ARG_0_.oldMethods.destroy(_ARG_0_, ...)
	if _ARG_0_._hud_timer then
		managers.hud:del_hud_timer(_ARG_0_._in_tab_screen, _ARG_0_._hud_timer)
		_ARG_0_._hud_timer = nil
	end
end

function TimerGui.update(_ARG_0_, ...)
	_ARG_0_.oldMethods.update(_ARG_0_, ...)
	if not _ARG_0_._jammed and _ARG_0_._hud_timer then
		_ARG_0_._hud_timer:set_completion(_ARG_0_._time_left, _ARG_0_._timer)
	end
end

function TimerGui._set_jammed(_ARG_0_, ...)
	_ARG_0_.oldMethods._set_jammed(_ARG_0_, ...)
	if _ARG_0_._hud_timer then
		_ARG_0_._hud_timer:set_jammed(_ARG_0_._jammed)
	end
end

function TimerGui.set_background_icons(_ARG_0_, _ARG_1_, ...)
	_ARG_0_:_create_hud_timer()
	if not _ARG_0_._hud_timer then
		return
	end

	for _FORV_6_, _FORV_7_ in pairs(_ARG_1_) do
		if _FORV_7_.texture:find("drillgui_icon_restarter") and _FORV_7_.color == _ARG_0_:get_upgrade_icon_color("upgrade_color_1") then
			_ARG_0_._hud_timer:set_timer_flash(tweak_data.hoxhud.drill_autorestart_flash)
			_ARG_0_._hud_timer:set_text_flash(tweak_data.hoxhud.drill_autorestart_flash)
		elseif _FORV_7_.texture:find("drillgui_icon_silent") then
			if _FORV_7_.color == _ARG_0_:get_upgrade_icon_color("upgrade_color_1") then
				_ARG_0_._hud_timer:set_text_color(tweak_data.hoxhud.drill_silent_basic)
			elseif _FORV_7_.color == _ARG_0_:get_upgrade_icon_color("upgrade_color_2") then
				_ARG_0_._hud_timer:set_text_color(tweak_data.hoxhud.drill_silent_aced)
			end

		end
	end

	_ARG_0_.oldMethods.set_background_icons(_ARG_0_, _ARG_1_, ...)
end

