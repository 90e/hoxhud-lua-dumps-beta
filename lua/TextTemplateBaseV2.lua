clone_methods(TextTemplateBase)
function TextTemplateBase._big_bank_welcome(_ARG_0_)
	if tweak_data.hoxhud.no_silly_stock_ticker then
		return _ARG_0_.oldMethods._big_bank_welcome(_ARG_0_)
	end

	_ARG_0_._unit:text_gui():set_row_speed(1, 500)
	_ARG_0_._unit:text_gui():set_row_speed(2, 240 + 120 * math.rand(1))
	_ARG_0_._unit:text_gui():set_row_gap(1, 320)
	for _FORV_4_ = 1, _ARG_0_._unit:text_gui().ROWS do
		_ARG_0_._unit:text_gui():clear_row_and_guis(_FORV_4_)
	end

	for _FORV_6_, _FORV_7_ in ipairs({
		"We're no strangers to love",
		"You know the rules, and so do I",
		"A full commitment's what I'm...",
		"...thinking of",
		"You wouldn't get this from any other guy.",
		"I just wanna tell you how I'm feeling",
		"Gotta make you understand",
		"Never gonna give you up",
		"Never gonna let you down",
		"Never gonna run around and desert you",
		"Never gonna make you cry",
		"Never gonna say goodbye",
		"Never gonna tell a lie and hurt you",
		" - "
	}) do
		_ARG_0_._unit:text_gui():add_text(1, _FORV_7_, "green")
		_ARG_0_._unit:text_gui():add_text(1, "-", "green")
		_ARG_0_._unit:text_gui():add_text(1, "Welcome to the Benevolent bank", "green")
		_ARG_0_._unit:text_gui():add_text(1, "-", "green")
	end

	for _FORV_6_, _FORV_7_ in ipairs({
		"1500 Penn.",
		"Coming here for your next big score?",
		"Guys, The Beast, go get it.",
		"Remember to keep bystanders away from The Beast",
		"they might notice the sound",
		"Why don't you just use a spoon?"
	}) do
		_ARG_0_._unit:text_gui():add_text(2, _FORV_7_, "light_green")
		_ARG_0_._unit:text_gui():add_text(2, " - ", "light_green")
	end
end

function TextTemplateBase._stock_ticker(_ARG_0_)
	if tweak_data.hoxhud.no_silly_stock_ticker then
		return _ARG_0_.oldMethods._stock_ticker(_ARG_0_)
	end

	for _FORV_4_ = 1, _ARG_0_._unit:text_gui().ROWS do
		_ARG_0_._unit:text_gui():set_row_gap(_FORV_4_, 20)
		_ARG_0_._unit:text_gui():clear_row_and_guis(_FORV_4_)
		_ARG_0_._unit:text_gui():set_row_speed(_FORV_4_, _FORV_4_ * 100 + 40 + 120 * math.rand(1))
	end

	_FOR_["Team$Evil"] = 1
	if not TextTemplateBase.STOCK_PERCENT then
		TextTemplateBase.STOCK_PERCENT = {}
		for _FORV_5_, _FORV_6_ in pairs({
			["HoxHud"] = 3,
			["OVERKILL Software"] = 1,
			["Almir's Beard"] = 1,
			["H.Y.P.E Trains Inc."] = 1,
			["Bodhi's Dry Cleaning"] = 1,
			["TEC"] = 3,
			["Juan in a Million Corp."] = 1,
			["Meth Cooking Benches Inc."] = 1,
			["James Hoxworth Records"] = 1,
			["Clone-A-Cop LLC."] = 1,
			["Bulletproof Shields Inc."] = 1,
			["Gage Ordnance Services"] = 1,
			["Los Dentistas Hermanos"] = 1,
			["Vlad's Cable Tie Mfg."] = 1,
			["Simmons Fusion Research Inc."] = 1,
			["HaxHud"] = 2,
			["Thermal Drilling Corp"] = 2,
			["Criminal Ambitions"] = 2,
			["Bodybag Mfg Corp."] = 2,
			["HL3 Confirmation Services"] = 2,
			["Mendoza Medical Supplies"] = 2
		}) do
			if (_FORV_6_ ~= 2 or not math.rand(-5, -55)) and (_FORV_6_ ~= 3 or not math.rand(9001, 9999)) then
			end

			TextTemplateBase.STOCK_PERCENT[_FORV_5_] = math.rand(5, 40)
		end
	end

	for _FORV_6_, _FORV_7_ in pairs({
		["HoxHud"] = 3,
		["OVERKILL Software"] = 1,
		["Almir's Beard"] = 1,
		["H.Y.P.E Trains Inc."] = 1,
		["Bodhi's Dry Cleaning"] = 1,
		["TEC"] = 3,
		["Juan in a Million Corp."] = 1,
		["Meth Cooking Benches Inc."] = 1,
		["James Hoxworth Records"] = 1,
		["Clone-A-Cop LLC."] = 1,
		["Bulletproof Shields Inc."] = 1,
		["Gage Ordnance Services"] = 1,
		["Los Dentistas Hermanos"] = 1,
		["Vlad's Cable Tie Mfg."] = 1,
		["Simmons Fusion Research Inc."] = 1,
		["HaxHud"] = 2,
		["Thermal Drilling Corp"] = 2,
		["Criminal Ambitions"] = 2,
		["Bodybag Mfg Corp."] = 2,
		["HL3 Confirmation Services"] = 2,
		["Mendoza Medical Supplies"] = 2
	}) do
		_ARG_0_._unit:text_gui():add_text(math.mod(1, _ARG_0_._unit:text_gui().ROWS) + 1, _FORV_6_, "white")
		_ARG_0_._unit:text_gui():add_text(math.mod(1, _ARG_0_._unit:text_gui().ROWS) + 1, "" .. (TextTemplateBase.STOCK_PERCENT[_FORV_6_] < 0 and "" or "+") .. string.format("%.2f", TextTemplateBase.STOCK_PERCENT[_FORV_6_]) .. "%", TextTemplateBase.STOCK_PERCENT[_FORV_6_] < 0 and "light_red" or "light_green", _ARG_0_._unit:text_gui().FONT_SIZE / 1.5, "bottom", nil)
		_ARG_0_._unit:text_gui():add_text(math.mod(1, _ARG_0_._unit:text_gui().ROWS) + 1, "  ", "white")
	end
end

