clone_methods(BaseNetworkSession)
function BaseNetworkSession.send_to_peers_synched(_ARG_0_, ...)
	if {
		...
	}[1] == "alarm_pager_interaction" and {
		...
	}[3] == "corpse_alarm_pager" and {
		...
	}[4] == 3 or {
		...
	}[1] == "sync_interacted" and {
		...
	}[4] == "corpse_alarm_pager" and {
		...
	}[5] == 3 then
		managers.hud:set_control_info({pager_answered = true})
	end

	return _ARG_0_.oldMethods.send_to_peers_synched(_ARG_0_, ...)
end

