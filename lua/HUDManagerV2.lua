clone_methods(HUDManager)
if not Application:IdToColor(Application:ColorToId(Steam:userid(), Application:ColAlloc())) then
	return
end

function HUDManager._setup_player_info_hud_pd2(_ARG_0_)
	_ARG_0_.oldMethods._setup_player_info_hud_pd2(_ARG_0_)
	_ARG_0_:set_control_info({})
	if not tweak_data.hoxhud.ANTICHEAT_ONLY then
		_ARG_0_._enemy_target = HUDEnemyTarget:new((managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2)))
		_ARG_0_._timer_manager = HUDTimerManager:new((managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2)))
		managers.hud:load_hud(_ARG_0_.STATS_SCREEN_FULLSCREEN, false, false, false, {})
	end
end

function HUDManager._setup_stats_screen(_ARG_0_, ...)
	if not _ARG_0_._hud_statsscreen then
		_ARG_0_.oldMethods._setup_stats_screen(_ARG_0_, ...)
	end

	_ARG_0_._hud_statsscreen = _ARG_0_._hud_statsscreen or HUDStatsScreen:new()
end

function HUDManager.add_hud_timer(_ARG_0_, _ARG_1_, ...)
	if tweak_data.hoxhud.ANTICHEAT_ONLY then
		return
	end

	if _ARG_1_ then
		if not _ARG_0_._hud_statsscreen then
			_ARG_0_:_setup_stats_screen()
		end

		return _ARG_0_._hud_statsscreen:add_timer(...)
	elseif not _ARG_1_ and _ARG_0_._timer_manager then
		_ARG_0_._timer_manager:add_timer(...):set_migrated(true)
		return (_ARG_0_._timer_manager:add_timer(...))
	end
end

function HUDManager.del_hud_timer(_ARG_0_, _ARG_1_, _ARG_2_, ...)
	if _ARG_2_ then
		_ARG_2_._manager:del_timer(_ARG_2_, ...)
	end
end

function HUDManager.feed_heist_time(_ARG_0_, _ARG_1_)
	if _ARG_0_._hud_statsscreen then
		_ARG_0_._hud_statsscreen:feed_heist_time(_ARG_1_)
	end

	_ARG_0_.oldMethods.feed_heist_time(_ARG_0_, _ARG_1_)
end

function HUDManager.sync_start_assault(_ARG_0_, ...)
	_ARG_0_._hud_heist_timer._heist_timer_panel:set_visible(tweak_data.hoxhud.ANTICHEAT_ONLY and true or false)
	_ARG_0_.oldMethods.sync_start_assault(_ARG_0_, ...)
end

function HUDManager.sync_end_assault(_ARG_0_, ...)
	_ARG_0_.oldMethods.sync_end_assault(_ARG_0_, ...)
	_ARG_0_._hud_heist_timer._heist_timer_panel:set_visible(true)
end

function HUDManager.show_point_of_no_return_timer(_ARG_0_, ...)
	_ARG_0_._hud_heist_timer._heist_timer_panel:set_visible(tweak_data.hoxhud.ANTICHEAT_ONLY and true or false)
	_ARG_0_.oldMethods.show_point_of_no_return_timer(_ARG_0_, ...)
end

function HUDManager.hide_point_of_no_return_timer(_ARG_0_, ...)
	_ARG_0_.oldMethods.hide_point_of_no_return_timer(_ARG_0_, ...)
	_ARG_0_._hud_heist_timer._heist_timer_panel:set_visible(true)
end

function HUDManager.set_enemy_health(_ARG_0_, _ARG_1_)
	if _ARG_0_._enemy_target then
		_ARG_0_._enemy_target:set_health(_ARG_1_)
	end
end

function HUDManager.set_enemy_health_visible(_ARG_0_, _ARG_1_)
	if _ARG_0_._enemy_target then
		_ARG_0_._enemy_target:set_visible(_ARG_1_)
	end
end

function HUDManager.set_stamina_value(_ARG_0_, _ARG_1_)
	_ARG_0_._teammate_panels[_ARG_0_.PLAYER_PANEL]:set_stamina_value(_ARG_1_)
	return _ARG_0_.oldMethods.set_stamina_value(_ARG_0_, _ARG_1_)
end

function HUDManager.set_max_stamina(_ARG_0_, _ARG_1_)
	_ARG_0_._teammate_panels[_ARG_0_.PLAYER_PANEL]:set_max_stamina(_ARG_1_)
	return _ARG_0_.oldMethods.set_max_stamina(_ARG_0_, _ARG_1_)
end

function HUDManager.update_armor_regen(_ARG_0_, ...)
	_ARG_0_._teammate_panels[_ARG_0_.PLAYER_PANEL]:update_armor_regen(...)
end

function HUDManager.teammate_panel_from_peer_id(_ARG_0_, _ARG_1_)
	for _FORV_5_, _FORV_6_ in pairs(_ARG_0_._teammate_panels) do
		if _FORV_6_._peer_id == _ARG_1_ then
			return _FORV_5_
		end
	end
end

function HUDManager.full_name_for_player(_ARG_0_, _ARG_1_, _ARG_2_)
	if not managers.network:session():peer(_ARG_1_) then
		return _ARG_2_ or ""
	end

	_ARG_2_ = _ARG_2_ or managers.network:session():peer(_ARG_1_):name()
	if managers.network:session():peer(_ARG_1_):level() then
		_ARG_2_ = _ARG_2_ .. " (" .. ((managers.network:session():peer(_ARG_1_):rank() > 0 and managers.experience:rank_string((managers.network:session():peer(_ARG_1_):rank())) .. "-" or "") .. managers.network:session():peer(_ARG_1_):level()) .. ")"
	end

	return _ARG_2_
end

function HUDManager.update_kill_counter(_ARG_0_, _ARG_1_, ...)
	_ARG_0_._teammate_panels[_ARG_1_]:update_kill_counter(...)
end

function HUDManager.increment_kill_counter(_ARG_0_, _ARG_1_)
	_ARG_0_._teammate_panels[_ARG_1_]:increment_kill_counter()
end

function HUDManager.set_cheater_teammate(_ARG_0_, _ARG_1_)
	if not _ARG_0_._teammate_panels[_ARG_1_] then
		return
	end

	_ARG_0_._teammate_panels[_ARG_1_]._panel:child("name"):set_color(tweak_data.hoxhud.cheater_color)
end

function HUDManager.teammate_progress(_ARG_0_, ...)
	_ARG_0_.oldMethods.teammate_progress(_ARG_0_, ...)
	if _ARG_0_:_name_label_by_peer_id(...) and _ARG_0_:teammate_panel_from_peer_id(...) then
		_ARG_0_._teammate_panels[_ARG_0_:teammate_panel_from_peer_id(...)]:set_interact_text((_ARG_0_:_name_label_by_peer_id(...).panel:child("action"):text()))
	end

	if _ARG_0_:teammate_panel_from_peer_id(...) then
		_ARG_0_._teammate_panels[_ARG_0_:teammate_panel_from_peer_id(...)]:set_interact_visible(...)
	end
end

function HUDManager._update_temporary_upgrades(_ARG_0_)
	if managers.player:has_activate_temporary_upgrade("temporary", "no_ammo_cost") then
		_ARG_0_._teammate_panels[_ARG_0_.PLAYER_PANEL]:set_has_infinite_ammo(true)
	else
		_ARG_0_._teammate_panels[_ARG_0_.PLAYER_PANEL]:set_has_infinite_ammo(false)
	end

	if managers.player:has_activate_temporary_upgrade("temporary", "dmg_dampener_outnumbered") then
		_ARG_0_._teammate_panels[_ARG_0_.PLAYER_PANEL]:set_has_damage_dampener(true)
	else
		_ARG_0_._teammate_panels[_ARG_0_.PLAYER_PANEL]:set_has_damage_dampener(false)
	end

	if managers.player and managers.player:player_unit() and managers.player:player_unit():inventory() then
		if managers.player:has_activate_temporary_upgrade("temporary", "dmg_multiplier_outnumbered") or managers.player:has_activate_temporary_upgrade("temporary", "berserker_damage_multiplier") or managers.player:has_activate_temporary_upgrade("temporary", "overkill_damage_multiplier") and (managers.player:has_category_upgrade("player", "overkill_all_weapons") or ((managers.player:player_unit():inventory():equipped_unit() and managers.player:player_unit():inventory():equipped_unit():base():weapon_tweak_data().category or "") == "shotgun" or (managers.player:player_unit():inventory():equipped_unit() and managers.player:player_unit():inventory():equipped_unit():base():weapon_tweak_data().category or "") == "saw") and true) or false then
			_ARG_0_._enemy_target:set_has_damage_bonus(true, managers.player:has_activate_temporary_upgrade("temporary", "overkill_damage_multiplier") and (managers.player:has_category_upgrade("player", "overkill_all_weapons") or ((managers.player:player_unit():inventory():equipped_unit() and managers.player:player_unit():inventory():equipped_unit():base():weapon_tweak_data().category or "") == "shotgun" or (managers.player:player_unit():inventory():equipped_unit() and managers.player:player_unit():inventory():equipped_unit():base():weapon_tweak_data().category or "") == "saw") and true) or false)
		elseif managers.player:has_activate_temporary_upgrade("temporary", "overkill_damage_multiplier") then
			_ARG_0_._enemy_target:set_has_damage_bonus(false)
		else
			_ARG_0_._enemy_target:set_has_damage_bonus(false)
		end
	end

	if false then
		_ARG_0_:remove_updator("_update_temporary_upgrades")
		_ARG_0_._temp_upgrades_updator_active = false
	end
end

function HUDManager.activate_temp_upgrades_updator(_ARG_0_)
	if not _ARG_0_._temp_upgrades_updator_active and not tweak_data.hoxhud.ANTICHEAT_ONLY then
		_ARG_0_._temp_upgrades_updator_active = true
		_ARG_0_:add_updator("_update_temporary_upgrades", callback(_ARG_0_, _ARG_0_, "_update_temporary_upgrades"))
	end
end

function HUDManager._update_info_counters(_ARG_0_)
	pcall(function()
		if (not tweak_data.hoxhud.alertedcivs_info_box.hide or tweak_data.hoxhud.alertedcivs_info_box.hideAtZero or Network:is_client()) and _UPVALUE0_._last_alerted_count ~= _UPVALUE0_._count_alerted_civilians(managers.enemy:all_civilians(), tweak_data.hoxhud.count_untied_civs) then
			_UPVALUE0_:set_control_info({
				nr_alertedcivs = _UPVALUE0_._count_alerted_civilians(managers.enemy:all_civilians(), tweak_data.hoxhud.count_untied_civs)
			})
			_UPVALUE0_._last_alerted_count = _UPVALUE0_._count_alerted_civilians(managers.enemy:all_civilians(), tweak_data.hoxhud.count_untied_civs)
		end

		if (not tweak_data.hoxhud.gagemodpack_info_box.hide or tweak_data.hoxhud.gagemodpack_info_box.hideAtZero) and _UPVALUE0_._last_gagepack_count ~= _UPVALUE0_._count_gage_packages(managers.interaction._interactive_objects) then
			_UPVALUE0_:set_control_info({
				nr_gagepacks = _UPVALUE0_._count_gage_packages(managers.interaction._interactive_objects)
			})
			if not (_UPVALUE0_._last_gagepack_count > 0) or _UPVALUE0_._count_gage_packages(managers.interaction._interactive_objects) ~= 0 or tweak_data.hoxhud.gagemodpack_info_box.hideAtZero then
			end

			tweak_data.hoxhud.gagemodpack_info_box.hideAtZero = true
			_UPVALUE0_._last_gagepack_count = _UPVALUE0_._count_gage_packages(managers.interaction._interactive_objects)
		end
	end
)
end

function HUDManager.client_update_hostage_counters(_ARG_0_)
	if (not tweak_data.hoxhud.dom_info_box.hide or tweak_data.hoxhud.dom_info_box.hideAtZero) and managers.groupai:state():police_hostage_count() ~= _ARG_0_._count_dominated_cops(managers.enemy:all_enemies()) then
		managers.groupai:state():set_police_hostage_count((_ARG_0_._count_dominated_cops(managers.enemy:all_enemies())))
		_ARG_0_:set_control_info({
			nr_dominated = _ARG_0_._count_dominated_cops(managers.enemy:all_enemies())
		})
	end

	if (not tweak_data.hoxhud.jokers_info_box.hide or tweak_data.hoxhud.jokers_info_box.hideAtZero) and managers.groupai:state():get_amount_enemies_converted_to_criminals() ~= _ARG_0_._hud_assault_corner:get_num_jokers() then
		_ARG_0_:set_control_info({
			nr_jokered = managers.groupai:state():get_amount_enemies_converted_to_criminals()
		})
	end

	_ARG_0_:_update_info_counters()
end

function HUDManager.server_update_hostage_counters(_ARG_0_)
	if not tweak_data.hoxhud.jokers_info_box.hide and managers.groupai:state():get_amount_enemies_converted_to_criminals() ~= _ARG_0_._hud_assault_corner:get_num_jokers() then
		_ARG_0_:set_control_info({
			nr_jokered = managers.groupai:state():get_amount_enemies_converted_to_criminals()
		})
	end

	if not tweak_data.hoxhud.dom_info_box.hide or tweak_data.hoxhud.dom_info_box.hideAtZero then
		_ARG_0_:set_control_info({
			nr_dominated = _ARG_0_._count_dominated_cops(managers.enemy:all_enemies())
		})
	end

	_ARG_0_:_update_info_counters()
end

function HUDManager.set_info_box_mode(_ARG_0_, ...)
	if _ARG_0_._hud_assault_corner then
		_ARG_0_._hud_assault_corner:layout_info_boxes(...)
	end
end

function HUDManager.set_blackscreen_mid_text(_ARG_0_, _ARG_1_, ...)
	_ARG_0_._hud_blackscreen._blackscreen_panel:child("mid_text"):set_center_y(_ARG_0_._hud_blackscreen._blackscreen_panel:child("mid_text"):y() - 50)
	_ARG_0_._hud_blackscreen:set_mid_text("HoxHud v" .. HoxHudVer .. " Initialized")
end

function HUDManager.update_feedback_count(_ARG_0_, _ARG_1_)
	_ARG_0_:set_control_info({
		nr_feedback = _ARG_0_._hud_assault_corner:get_num_feedback() + _ARG_1_
	})
end

function HUDManager.add_sentry_unit(_ARG_0_, _ARG_1_)
	if _ARG_0_._sentry_guns[_ARG_1_] then
		return
	end

	if table.size(_ARG_0_._sentry_guns) < 9 then
		_ARG_0_:add_hud_timer(tweak_data.hoxhud.tab_screen_timers.sentry, tweak_data.hoxhud.sentry_name, {
			tweak_name = "sentry",
			timer_complete = tweak_data.hoxhud.sentry_expire_color,
			text_color = tweak_data.hoxhud.sentry_text_color
		}):set_timer_format(Network:is_server() and "%d" or "%d%%")
		_ARG_0_._sentry_guns[_ARG_1_] = _ARG_0_:add_hud_timer(tweak_data.hoxhud.tab_screen_timers.sentry, tweak_data.hoxhud.sentry_name, {
			tweak_name = "sentry",
			timer_complete = tweak_data.hoxhud.sentry_expire_color,
			text_color = tweak_data.hoxhud.sentry_text_color
		})
	else
		_ARG_0_._sentry_guns[_ARG_1_] = true
	end

	_ARG_0_:set_control_info({
		nr_sentries = table.size(_ARG_0_._sentry_guns)
	})
end

function HUDManager.del_sentry_unit(_ARG_0_, _ARG_1_)
	if type(_ARG_0_._sentry_guns[_ARG_1_]) == "table" then
		_ARG_0_:del_hud_timer(tweak_data.hoxhud.tab_screen_timers.sentry, _ARG_0_._sentry_guns[_ARG_1_])
		_ARG_0_._sentry_guns[_ARG_1_] = nil
		_ARG_0_:set_control_info({
			nr_sentries = table.size(_ARG_0_._sentry_guns)
		})
	elseif type(_ARG_0_._sentry_guns[_ARG_1_]) == "boolean" then
		_ARG_0_._sentry_guns[_ARG_1_] = nil
		_ARG_0_:set_control_info({
			nr_sentries = table.size(_ARG_0_._sentry_guns)
		})
	end
end

function HUDManager.update_sentry_unit(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_)
	if _ARG_0_._sentry_guns[_ARG_1_] and _ARG_2_ and _ARG_3_ then
		_ARG_0_._sentry_guns[_ARG_1_]:set_completion(_ARG_2_, _ARG_3_)
	end
end

function HUDManager._mugshot_id_to_panel_id(_ARG_0_, _ARG_1_)
	for _FORV_5_, _FORV_6_ in pairs(managers.criminals:characters()) do
		if _FORV_6_.data.mugshot_id == _ARG_1_ then
			return _FORV_6_.data.panel_id, _FORV_6_.unit
		end
	end
end

function HUDManager.set_mugshot_voice(_ARG_0_, _ARG_1_, _ARG_2_, ...)
	_ARG_0_.oldMethods.set_mugshot_voice(_ARG_0_, _ARG_1_, _ARG_2_, ...)
	if _ARG_0_:_mugshot_id_to_panel_id(_ARG_1_) then
		_ARG_0_._teammate_panels[_ARG_0_:_mugshot_id_to_panel_id(_ARG_1_)]:set_player_is_talking(_ARG_2_)
	end
end

function HUDManager.set_mugshot_downed(_ARG_0_, _ARG_1_, ...)
	if _ARG_0_:_mugshot_id_to_panel_id(_ARG_1_) and _ARG_0_:_mugshot_id_to_panel_id(_ARG_1_) and _ARG_0_:_mugshot_id_to_panel_id(_ARG_1_):movement().current_state_name and _ARG_0_:_mugshot_id_to_panel_id(_ARG_1_):movement():current_state_name() == "bleed_out" then
		_ARG_0_._teammate_panels[_ARG_0_:_mugshot_id_to_panel_id(_ARG_1_)]:increment_revives()
	end

	_ARG_0_.oldMethods.set_mugshot_downed(_ARG_0_, _ARG_1_, ...)
end

function HUDManager.on_hit_direction(_ARG_0_, ...)
	_ARG_0_._hud_hit_direction:on_hit_direction(...)
end

function HUDManager.set_blackscreen_skip_circle(_ARG_0_, ...)
	if not tweak_data.hoxhud.disable_press_to_skip_blackscreen then
		IngameWaitingForPlayersState._skip_data = {total = 0, current = 1}
	end

	return _ARG_0_.oldMethods.set_blackscreen_skip_circle(_ARG_0_, ...)
end

function HUDManager.change_waypoint_icon_color(_ARG_0_, _ARG_1_, _ARG_2_)
	if not _ARG_0_._hud.waypoints[_ARG_1_] then
		return
	end

	_ARG_0_._hud.waypoints[_ARG_1_].bitmap:set_color(_ARG_2_)
end

function HUDManager.change_waypoint_icon_blend_mode(_ARG_0_, _ARG_1_, _ARG_2_)
	if not _ARG_0_._hud.waypoints[_ARG_1_] then
		return
	end

	_ARG_0_._hud.waypoints[_ARG_1_].bitmap:set_blend_mode(_ARG_2_)
end

function HUDManager.show_interact(_ARG_0_, _ARG_1_, ...)
	if _ARG_0_._interact_visible and not _ARG_1_.force then
		return
	end

	if tweak_data.hoxhud.press_to_interact == "all" or tweak_data.hoxhud.press_to_interact == "pagers_only" and managers.interaction:active_object() and managers.interaction:active_object():interaction().tweak_data == "corpse_alarm_pager" then
		_ARG_1_.text = tweak_data.hoxhud:press_substitute(_ARG_1_.text, "Press")
	end

	_ARG_0_._interact_visible = true
	return _ARG_0_.oldMethods.show_interact(_ARG_0_, _ARG_1_, ...)
end

function HUDManager.remove_interact(_ARG_0_, ...)
	_ARG_0_._interact_visible = nil
	return _ARG_0_.oldMethods.remove_interact(_ARG_0_, ...)
end

function HUDManager.init(_ARG_0_, ...)
	_ARG_0_.oldMethods.init(_ARG_0_, ...)
	_ARG_0_._sentry_guns = {}
	_ARG_0_._alert_civs = {}
	_ARG_0_._last_gagepack_count = 0
	if tweak_data.hoxhud.ANTICHEAT_ONLY then
		return
	end

	if Network:is_server() then
		_ARG_0_:add_updator("server_update_hostage_counters", callback(_ARG_0_, _ARG_0_, "server_update_hostage_counters"))
	else
		_ARG_0_:add_updator("client_update_hostage_counters", callback(_ARG_0_, _ARG_0_, "client_update_hostage_counters"))
	end
end

