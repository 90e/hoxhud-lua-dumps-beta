clone_methods(GroupAIStateBase)
function GroupAIStateBase.on_hostage_state(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	_ARG_0_.oldMethods.on_hostage_state(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	if _ARG_3_ then
		managers.hud:set_control_info({
			nr_hostages = _ARG_0_:hostage_count(),
			nr_dominated = _ARG_0_:police_hostage_count()
		})
	end
end

function GroupAIStateBase.set_police_hostage_count(_ARG_0_, _ARG_1_)
	_ARG_0_._police_hostage_headcount = _ARG_1_
end

function GroupAIStateBase.convert_hostage_to_criminal(_ARG_0_, ...)
	managers.hud:set_control_info({
		nr_jokered = _ARG_0_:get_amount_enemies_converted_to_criminals() or 0
	})
	return (_ARG_0_.oldMethods.convert_hostage_to_criminal(_ARG_0_, ...))
end

function GroupAIStateBase.sync_converted_enemy(_ARG_0_, ...)
	managers.hud:set_control_info({
		nr_jokered = _ARG_0_:get_amount_enemies_converted_to_criminals() or 0
	})
	return (_ARG_0_.oldMethods.sync_converted_enemy(_ARG_0_, ...))
end

function GroupAIStateBase.clbk_minion_dies(_ARG_0_, ...)
	_ARG_0_.oldMethods.clbk_minion_dies(_ARG_0_, ...)
	managers.hud:set_control_info({
		nr_jokered = _ARG_0_:get_amount_enemies_converted_to_criminals() or 0
	})
end

function GroupAIStateBase.set_whisper_mode(_ARG_0_, ...)
	_ARG_0_.oldMethods.set_whisper_mode(_ARG_0_, ...)
	if (...) then
		managers.hud:set_info_box_mode("stealth")
	else
		managers.hud:set_info_box_mode("loud")
	end
end

function GroupAIStateBase.is_pacified_civilian(_ARG_0_, _ARG_1_)
	if not _ARG_1_:anim_data().tied and not _ARG_1_:contour()._contour_list then
		return false
	end

	return true
end

function GroupAIStateBase._upd_criminal_suspicion_progress(_ARG_0_, ...)
	_ARG_0_.oldMethods._upd_criminal_suspicion_progress(_ARG_0_, ...)
	if not next(_ARG_0_._suspicion_hud_data) or not _ARG_0_._ai_enabled then
		return
	end

	for _FORV_6_, _FORV_7_ in pairs(_ARG_0_._suspicion_hud_data) do
		if _FORV_7_ and (_FORV_7_.status == true or _FORV_7_.status == "call_interrupted") and managers.enemy:is_civilian(_FORV_7_.u_observer) then
			if not _FORV_7_.is_pacified and _ARG_0_:is_pacified_civilian(_FORV_7_.u_observer) then
				managers.hud:change_waypoint_icon("susp1" .. tostring(_FORV_7_.u_observer:key()), tweak_data.hoxhud.civilian_pacified_icon or "pd2_generic_look")
				managers.hud:change_waypoint_arrow_color("susp1" .. tostring(_FORV_7_.u_observer:key()), tweak_data.hoxhud.civilian_pacified_arrow_alert_color)
				managers.hud:change_waypoint_icon_color("susp1" .. tostring(_FORV_7_.u_observer:key()), tweak_data.hoxhud.civilian_pacified_icon_alert_color)
				_FORV_7_.is_pacified = true
			elseif _FORV_7_.is_pacified and not _ARG_0_:is_pacified_civilian(_FORV_7_.u_observer) then
				managers.hud:change_waypoint_icon("susp1" .. tostring(_FORV_7_.u_observer:key()), "wp_detected")
				managers.hud:change_waypoint_arrow_color("susp1" .. tostring(_FORV_7_.u_observer:key()), tweak_data.hud.detected_color)
				managers.hud:change_waypoint_icon_color("susp1" .. tostring(_FORV_7_.u_observer:key()), tweak_data.hud.detected_color)
				_FORV_7_.is_pacified = nil
			end

		end
	end
end

