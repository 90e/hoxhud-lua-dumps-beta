clone_methods(SentryGunDamage)
function SentryGunDamage.die(_ARG_0_, ...)
	managers.hud:del_sentry_unit(_ARG_0_._unit)
	_ARG_0_.oldMethods.die(_ARG_0_, ...)
end

function SentryGunDamage.destroy(_ARG_0_, ...)
	managers.hud:del_sentry_unit(_ARG_0_._unit)
	_ARG_0_.oldMethods.destroy(_ARG_0_, ...)
end

