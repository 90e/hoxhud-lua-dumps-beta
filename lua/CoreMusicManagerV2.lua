clone_methods(CoreMusicManager)
function CoreMusicManager.set_volume(_ARG_0_, _ARG_1_, ...)
	if IngameWaitingForPlayersState and IngameWaitingForPlayersState._custom_music then
		IngameWaitingForPlayersState._custom_music:set_volume(_ARG_1_ * 100)
	end

	return _ARG_0_.oldMethods.set_volume(_ARG_0_, _ARG_1_, ...)
end

