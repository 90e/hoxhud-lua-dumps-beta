clone_methods(LootManager)
function LootManager.sync_load(_ARG_0_, ...)
	_ARG_0_.oldMethods.sync_load(_ARG_0_, ...)
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_0_._global.secured) do
		_FORV_7_.multiplier = (_FORV_7_.multiplier > tweak_data.upgrades.values.player.small_loot_multiplier[#tweak_data.upgrades.values.player.small_loot_multiplier] or _FORV_7_.multiplier < 1) and 1 or _FORV_7_.multiplier
	end
end

