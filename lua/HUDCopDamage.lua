HUDCopDamage = class()
function HUDCopDamage.init(_ARG_0_, _ARG_1_)
	_ARG_0_._unit = _ARG_1_
end

function HUDCopDamage.show_damage(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_)
	if tweak_data.hoxhud.disable_damage_counter or tweak_data.hoxhud.dmg_counter_ignore_civilians and alive(_ARG_0_._unit) and managers.enemy:is_civilian(_ARG_0_._unit) then
		return
	end

	if _ARG_0_._ws and not tweak_data.hoxhud.show_damage_per_hit then
		_ARG_0_._ws:panel():stop()
		World:newgui():destroy_workspace(_ARG_0_._ws)
		_ARG_1_ = _ARG_1_ + _ARG_0_._damage
	end

	table.insert({}, World:newgui():create_world_workspace(tweak_data.hoxhud.workspace_size[1], tweak_data.hoxhud.workspace_size[2], _ARG_0_._unit:movement():m_head_pos() + tweak_data.hoxhud.unit_offset, Vector3(tweak_data.hoxhud.text_scaling, math.atan2(managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().y, managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().x) / 2, 0), Vector3(0, 0, -tweak_data.hoxhud.text_scaling)):panel():panel({
		visible = true,
		name = "text_panel",
		layer = 0
	}):text({
		text = _ARG_2_ and (tweak_data.hoxhud.show_multiplied_enemy_health and string.format("%d", _ARG_1_ * 10) or string.format("%.1f", _ARG_1_)) .. " " .. managers.localization:get_default_macro("BTN_SKULL") or tweak_data.hoxhud.show_multiplied_enemy_health and string.format("%d", _ARG_1_ * 10) or string.format("%.1f", _ARG_1_),
		align = "left",
		vertical = "bottom",
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.hoxhud.text_font_size,
		color = Color.black,
		layer = 2,
		color = _ARG_2_ and tweak_data.hoxhud.fatal_hit_color or tweak_data.hoxhud.non_fatal_hit_color,
		layer = 3
	}))
	table.insert({}, World:newgui():create_world_workspace(tweak_data.hoxhud.workspace_size[1], tweak_data.hoxhud.workspace_size[2], _ARG_0_._unit:movement():m_head_pos() + tweak_data.hoxhud.unit_offset, Vector3(tweak_data.hoxhud.text_scaling, math.atan2(managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().y, managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().x) / 2, 0), Vector3(0, 0, -tweak_data.hoxhud.text_scaling)):panel():panel({
		visible = true,
		name = "text_panel",
		layer = 0
	}):text({
		text = _ARG_2_ and (tweak_data.hoxhud.show_multiplied_enemy_health and string.format("%d", _ARG_1_ * 10) or string.format("%.1f", _ARG_1_)) .. " " .. managers.localization:get_default_macro("BTN_SKULL") or tweak_data.hoxhud.show_multiplied_enemy_health and string.format("%d", _ARG_1_ * 10) or string.format("%.1f", _ARG_1_),
		align = "left",
		vertical = "bottom",
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.hoxhud.text_font_size,
		color = Color.black,
		layer = 2,
		color = _ARG_2_ and tweak_data.hoxhud.fatal_hit_color or tweak_data.hoxhud.non_fatal_hit_color,
		layer = 3
	}))
	table.insert({}, World:newgui():create_world_workspace(tweak_data.hoxhud.workspace_size[1], tweak_data.hoxhud.workspace_size[2], _ARG_0_._unit:movement():m_head_pos() + tweak_data.hoxhud.unit_offset, Vector3(tweak_data.hoxhud.text_scaling, math.atan2(managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().y, managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().x) / 2, 0), Vector3(0, 0, -tweak_data.hoxhud.text_scaling)):panel():panel({
		visible = true,
		name = "text_panel",
		layer = 0
	}):text({
		text = _ARG_2_ and (tweak_data.hoxhud.show_multiplied_enemy_health and string.format("%d", _ARG_1_ * 10) or string.format("%.1f", _ARG_1_)) .. " " .. managers.localization:get_default_macro("BTN_SKULL") or tweak_data.hoxhud.show_multiplied_enemy_health and string.format("%d", _ARG_1_ * 10) or string.format("%.1f", _ARG_1_),
		align = "left",
		vertical = "bottom",
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.hoxhud.text_font_size,
		color = Color.black,
		layer = 2,
		color = _ARG_2_ and tweak_data.hoxhud.fatal_hit_color or tweak_data.hoxhud.non_fatal_hit_color,
		layer = 3
	}))
	table.insert({}, World:newgui():create_world_workspace(tweak_data.hoxhud.workspace_size[1], tweak_data.hoxhud.workspace_size[2], _ARG_0_._unit:movement():m_head_pos() + tweak_data.hoxhud.unit_offset, Vector3(tweak_data.hoxhud.text_scaling, math.atan2(managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().y, managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().x) / 2, 0), Vector3(0, 0, -tweak_data.hoxhud.text_scaling)):panel():panel({
		visible = true,
		name = "text_panel",
		layer = 0
	}):text({
		text = _ARG_2_ and (tweak_data.hoxhud.show_multiplied_enemy_health and string.format("%d", _ARG_1_ * 10) or string.format("%.1f", _ARG_1_)) .. " " .. managers.localization:get_default_macro("BTN_SKULL") or tweak_data.hoxhud.show_multiplied_enemy_health and string.format("%d", _ARG_1_ * 10) or string.format("%.1f", _ARG_1_),
		align = "left",
		vertical = "bottom",
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.hoxhud.text_font_size,
		color = Color.black,
		layer = 2,
		color = _ARG_2_ and tweak_data.hoxhud.fatal_hit_color or tweak_data.hoxhud.non_fatal_hit_color,
		layer = 3
	}))
	{}[1]:set_x({}[1]:x() - 1)
	{}[1]:set_y({}[1]:y() - 1)
	{}[2]:set_x({}[2]:x() + 1)
	{}[2]:set_y({}[2]:y() + 1)
	{}[3]:set_x({}[3]:x() + 1)
	{}[3]:set_y({}[3]:y() - 1)
	{}[4]:set_x({}[4]:x() - 1)
	{}[4]:set_y({}[4]:y() + 1)
	if _ARG_2_ and _ARG_3_ then
	end

	table.insert({}, World:newgui():create_world_workspace(tweak_data.hoxhud.workspace_size[1], tweak_data.hoxhud.workspace_size[2], _ARG_0_._unit:movement():m_head_pos() + tweak_data.hoxhud.unit_offset, Vector3(tweak_data.hoxhud.text_scaling, math.atan2(managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().y, managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().x) / 2, 0), Vector3(0, 0, -tweak_data.hoxhud.text_scaling)):panel():panel({
		visible = true,
		name = "text_panel",
		layer = 0
	}):text({
		text = _ARG_2_ and (tweak_data.hoxhud.show_multiplied_enemy_health and string.format("%d", _ARG_1_ * 10) or string.format("%.1f", _ARG_1_)) .. " " .. managers.localization:get_default_macro("BTN_SKULL") or tweak_data.hoxhud.show_multiplied_enemy_health and string.format("%d", _ARG_1_ * 10) or string.format("%.1f", _ARG_1_),
		align = "left",
		vertical = "bottom",
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.hoxhud.text_font_size,
		color = Color.black,
		layer = 2,
		color = _ARG_2_ and tweak_data.hoxhud.fatal_hit_color or tweak_data.hoxhud.non_fatal_hit_color,
		layer = 3
	}))
	World:newgui():create_world_workspace(tweak_data.hoxhud.workspace_size[1], tweak_data.hoxhud.workspace_size[2], _ARG_0_._unit:movement():m_head_pos() + tweak_data.hoxhud.unit_offset, Vector3(tweak_data.hoxhud.text_scaling, math.atan2(managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().y, managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().x) / 2, 0), Vector3(0, 0, -tweak_data.hoxhud.text_scaling)):panel():panel({
		visible = true,
		name = "text_panel",
		layer = 0
	}):animate(callback(_ARG_0_, _ARG_0_, "_animate_dmg"), World:newgui():create_world_workspace(tweak_data.hoxhud.workspace_size[1], tweak_data.hoxhud.workspace_size[2], _ARG_0_._unit:movement():m_head_pos() + tweak_data.hoxhud.unit_offset, Vector3(tweak_data.hoxhud.text_scaling, math.atan2(managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().y, managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().x) / 2, 0), Vector3(0, 0, -tweak_data.hoxhud.text_scaling)), {}, _ARG_0_._unit:movement():m_head_pos() + tweak_data.hoxhud.unit_offset, _ARG_2_, _ARG_3_, (World:newgui():create_world_workspace(tweak_data.hoxhud.workspace_size[1], tweak_data.hoxhud.workspace_size[2], _ARG_0_._unit:movement():m_head_pos() + tweak_data.hoxhud.unit_offset, Vector3(tweak_data.hoxhud.text_scaling, math.atan2(managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().y, managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().x) / 2, 0), Vector3(0, 0, -tweak_data.hoxhud.text_scaling)):panel():panel({
		visible = true,
		name = "glow_panel",
		layer = 0
	}):bitmap({
		name = "glow",
		texture = "guis/textures/pd2/crimenet_marker_glow",
		x = tweak_data.hoxhud.flash_offset[1],
		y = tweak_data.hoxhud.flash_offset[2],
		texture_rect = {
			1,
			1,
			62,
			62
		},
		h = tweak_data.hoxhud.flash_dimensions[1],
		w = tweak_data.hoxhud.flash_dimensions[2],
		color = tweak_data.hoxhud.headshot_kill_flash_color,
		blend_mode = "add",
		layer = 1,
		align = "left",
		visible = true,
		rotation = 360
	})))
	_ARG_0_._ws = World:newgui():create_world_workspace(tweak_data.hoxhud.workspace_size[1], tweak_data.hoxhud.workspace_size[2], _ARG_0_._unit:movement():m_head_pos() + tweak_data.hoxhud.unit_offset, Vector3(tweak_data.hoxhud.text_scaling, math.atan2(managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().y, managers.player:player_unit():camera():position() - _ARG_0_._unit:movement():m_head_pos().x) / 2, 0), Vector3(0, 0, -tweak_data.hoxhud.text_scaling))
	_ARG_0_._damage = _ARG_1_
end

function HUDCopDamage._animate_dmg(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_)
	while tweak_data.hoxhud.hit_display_duration > 0 do
		for _FORV_18_, _FORV_19_ in ipairs(_ARG_3_) do
			_FORV_19_:set_alpha((tweak_data.hoxhud.hit_display_duration - (0 + coroutine.yield())) / tweak_data.hoxhud.hit_display_duration)
		end

		if _ARG_5_ and _ARG_6_ then
			if tweak_data.hoxhud.fabulousdimitry then
				_ARG_3_[5]:set_color(Color(math.sin(405 * (0 + coroutine.yield()) + 0) / 2 + 0.5, math.sin(420 * (0 + coroutine.yield()) + 60) / 2 + 0.5, math.sin(435 * (0 + coroutine.yield()) + 120) / 2 + 0.5))
				_ARG_7_:set_color(Color(math.sin(435 * (0 + coroutine.yield()) + 120) / 2 + 0.5, math.cos(420 * (0 + coroutine.yield()) + 60) / 2 + 0.5, math.sin(405 * (0 + coroutine.yield()) + 0) / 2 + 0.5))
			end

			_ARG_7_:set_alpha(math.abs(math.sin(360 * (0 + coroutine.yield()))) * ((tweak_data.hoxhud.hit_display_duration - (0 + coroutine.yield())) / tweak_data.hoxhud.hit_display_duration))
		end

		_ARG_4_ = alive(_ARG_0_._unit) and _ARG_0_._unit:movement():m_head_pos() + tweak_data.hoxhud.unit_offset or _ARG_4_
		_ARG_2_:set_world(tweak_data.hoxhud.workspace_size[1], tweak_data.hoxhud.workspace_size[2], _ARG_4_ + (_ARG_5_ and Vector3(0, 0, 0) + tweak_data.hoxhud.fatal_hit_change_per_tick or Vector3(0, 0, 0) + tweak_data.hoxhud.normal_hit_change_per_tick), Vector3(tweak_data.hoxhud.text_scaling, 90 - (90 - (0 > math.atan2(managers.player:player_unit():camera():position() - _ARG_4_.y, managers.player:player_unit():camera():position() - _ARG_4_.x) and math.atan2(managers.player:player_unit():camera():position() - _ARG_4_.y, managers.player:player_unit():camera():position() - _ARG_4_.x) - 90 or math.atan2(managers.player:player_unit():camera():position() - _ARG_4_.y, managers.player:player_unit():camera():position() - _ARG_4_.x) + 90)) % 180 < -90 and 90 - (90 - (0 > math.atan2(managers.player:player_unit():camera():position() - _ARG_4_.y, managers.player:player_unit():camera():position() - _ARG_4_.x) and math.atan2(managers.player:player_unit():camera():position() - _ARG_4_.y, managers.player:player_unit():camera():position() - _ARG_4_.x) - 90 or math.atan2(managers.player:player_unit():camera():position() - _ARG_4_.y, managers.player:player_unit():camera():position() - _ARG_4_.x) + 90)) % 180 + 180 or 90 - (90 - (0 > math.atan2(managers.player:player_unit():camera():position() - _ARG_4_.y, managers.player:player_unit():camera():position() - _ARG_4_.x) and math.atan2(managers.player:player_unit():camera():position() - _ARG_4_.y, managers.player:player_unit():camera():position() - _ARG_4_.x) - 90 or math.atan2(managers.player:player_unit():camera():position() - _ARG_4_.y, managers.player:player_unit():camera():position() - _ARG_4_.x) + 90)) % 180 - 180 or 90 - (90 - (0 > math.atan2(managers.player:player_unit():camera():position() - _ARG_4_.y, managers.player:player_unit():camera():position() - _ARG_4_.x) and math.atan2(managers.player:player_unit():camera():position() - _ARG_4_.y, managers.player:player_unit():camera():position() - _ARG_4_.x) - 90 or math.atan2(managers.player:player_unit():camera():position() - _ARG_4_.y, managers.player:player_unit():camera():position() - _ARG_4_.x) + 90)) % 180, 0), Vector3(0, 0, -tweak_data.hoxhud.text_scaling))
		if 0 < (0 > math.atan2(managers.player:player_unit():camera():position() - _ARG_4_.y, managers.player:player_unit():camera():position() - _ARG_4_.x) and math.atan2(managers.player:player_unit():camera():position() - _ARG_4_.y, managers.player:player_unit():camera():position() - _ARG_4_.x) - 90 or math.atan2(managers.player:player_unit():camera():position() - _ARG_4_.y, managers.player:player_unit():camera():position() - _ARG_4_.x) + 90) then
			_ARG_2_:mirror_x()
		end
	end

	_ARG_0_._ws = nil
	World:newgui():destroy_workspace(_ARG_2_)
end

