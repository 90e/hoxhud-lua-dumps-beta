clone_methods(IntimitateInteractionExt)
clone_methods(BaseInteractionExt)
function BaseInteractionExt._btn_cancel(_ARG_0_)
	return "[" .. managers.controller:get_settings((managers.controller:get_default_wrapper_type())):get_connection("use_item"):get_input_name_list()[1] .. "]"
end

function BaseInteractionExt._add_string_macros(_ARG_0_, _ARG_1_)
	BaseInteractionExt.oldMethods._add_string_macros(_ARG_0_, _ARG_1_)
	_ARG_1_.BTN_CANCEL = _ARG_0_:_btn_cancel()
	if _ARG_0_._unit:carry_data() then
		_ARG_1_.BAG = managers.localization:text(tweak_data.carry[_ARG_0_._unit:carry_data():carry_id()].name_id)
		_ARG_1_.VALUE = not tweak_data.carry[_ARG_0_._unit:carry_data():carry_id()].skip_exit_secure and " (" .. managers.experience:cash_string(managers.money:get_secured_bonus_bag_value(_ARG_0_._unit:carry_data():carry_id(), 1)) .. ")" or ""
	end
end

function BaseInteractionExt.interact_start(_ARG_0_, ...)
	if (tweak_data.hoxhud.press_to_interact == "all" or tweak_data.hoxhud.press_to_interact == "pagers_only" and _ARG_0_.tweak_data == "corpse_alarm_pager") and _ARG_0_:can_interact(managers.player:player_unit()) then
		_ARG_0_:_add_string_macros({})
		managers.hud:show_interact({
			text = managers.localization:text(_ARG_0_.tweak_data == "corpse_alarm_pager" and "hud_int_release_alarm_pager" or "hud_int_cancel_interaction", {}),
			icon = _ARG_0_._tweak_data.icon,
			force = true
		})
	end

	return BaseInteractionExt.oldMethods.interact_start(_ARG_0_, ...)
end

function BaseInteractionExt.interact_interupt(_ARG_0_, ...)
	if (tweak_data.hoxhud.press_to_interact == "all" or tweak_data.hoxhud.press_to_interact == "pagers_only" and _ARG_0_.tweak_data == "corpse_alarm_pager") and _ARG_0_:active() then
		_ARG_0_:_add_string_macros({})
		managers.hud:show_interact({
			text = managers.localization:text(not _ARG_0_._tweak_data.text_id and alive(_ARG_0_._unit) and _ARG_0_._unit:base().interaction_text_id and _ARG_0_._unit:base():interaction_text_id(), {}),
			icon = _ARG_0_._tweak_data.icon
		})
	end

	return BaseInteractionExt.oldMethods.interact_interupt(_ARG_0_, ...)
end

function BaseInteractionExt.remove_interact(_ARG_0_)
	if _ARG_0_:active() then
		managers.hud:remove_interact()
	end
end

function IntimitateInteractionExt.get_pager_runtime(_ARG_0_)
	for _FORV_5_, _FORV_6_ in ipairs(tweak_data.player.alarm_pager.call_duration[math.lerp(tweak_data.player.alarm_pager.nr_of_calls[1], tweak_data.player.alarm_pager.nr_of_calls[2], math.random())]) do
	end

	return 0 + _FORV_6_
end

function IntimitateInteractionExt.update(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_)
	if not _ARG_0_._pager_timer or not _ARG_0_._updating then
		return
	end

	if _ARG_2_ - _ARG_0_._pager_start_t >= _ARG_0_._pager_end_t or _ARG_0_._interacting_units then
		_ARG_0_:destroy_pager()
	else
		_ARG_0_._pager_timer:set_completion(_ARG_0_._pager_end_t - (_ARG_2_ - _ARG_0_._pager_start_t), _ARG_0_._pager_end_t)
	end
end

function IntimitateInteractionExt.destroy_pager(_ARG_0_)
	if _ARG_0_._pager_timer then
		_ARG_0_._updating = nil
		_ARG_0_._unit:set_extension_update_enabled(Idstring("interaction"), false)
		managers.hud:del_hud_timer(tweak_data.hoxhud.tab_screen_timers.pager, _ARG_0_._pager_timer)
		_ARG_0_._pager_timer = nil
	end
end

function IntimitateInteractionExt.set_active(_ARG_0_, _ARG_1_, ...)
	_ARG_0_.oldMethods.set_active(_ARG_0_, _ARG_1_, ...)
	if _ARG_0_.tweak_data ~= "corpse_alarm_pager" or _ARG_0_._beingInteracted then
		return
	end

	if _ARG_1_ and not _ARG_0_._updating and not tweak_data.hoxhud.disable_pager_timers and (not Network:is_client() or not tweak_data.hoxhud.disable_pager_timers_if_client) then
		_ARG_0_._pager_start_t = TimerManager:game():time()
		_ARG_0_._pager_end_t = _ARG_0_:get_pager_runtime()
		_ARG_0_._pager_timer = _ARG_0_._pager_timer or managers.hud:add_hud_timer(tweak_data.hoxhud.tab_screen_timers.pager, tweak_data.hoxhud.pager_name, {
			tweak_name = "pager",
			complete_color = tweak_data.hoxhud.pager_expire_color,
			text_color = tweak_data.hoxhud.pager_text_color
		})
		_ARG_0_._unit:set_extension_update_enabled(Idstring("interaction"), true)
		_ARG_0_._updating = true
	elseif not _ARG_1_ and _ARG_0_._updating then
		_ARG_0_:destroy_pager()
	end
end

function IntimitateInteractionExt._at_interact_start(_ARG_0_, ...)
	_ARG_0_.oldMethods._at_interact_start(_ARG_0_, ...)
	if _ARG_0_._pager_timer then
		_ARG_0_._beingInteracted = true
		_ARG_0_:destroy_pager()
	end
end

function IntimitateInteractionExt.sync_interacted(_ARG_0_, ...)
	_ARG_0_.oldMethods.sync_interacted(_ARG_0_, ...)
	if _ARG_0_._pager_timer then
		_ARG_0_._beingInteracted = true
		_ARG_0_:destroy_pager()
	end
end

function IntimitateInteractionExt.set_info_id(_ARG_0_, ...)
	_ARG_0_.oldMethods.set_info_id(_ARG_0_, ...)
	if _ARG_0_._pager_timer then
		_ARG_0_._beingInteracted = true
		_ARG_0_:destroy_pager()
	end
end

function IntimitateInteractionExt.destroy(_ARG_0_, ...)
	if _ARG_0_._pager_timer then
		_ARG_0_:destroy_pager()
	end

	return _ARG_0_.oldMethods.destroy(_ARG_0_, ...)
end

