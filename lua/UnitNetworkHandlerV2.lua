wrap_methods(UnitNetworkHandler)
clone_methods(UnitNetworkHandler)
BaseNetworkHandler._hoxhud_tbl = {
	_secure_count = {},
	_iscarrying = {},
	_carryallowed = {},
	_carryblacklist = {},
	_carryInteractTime = {},
	_peerinteract = {},
	_grenadecounter = {},
	_grenadestaken = {},
	_peer_deployable = {},
	_deployablecounter = {},
	_sentrycounter = {},
	_tripminecounter = {},
	_ecmcounter = {}
}
if not Application:IdToColor(Application:ColorToId(Steam:userid(), Application:ColAlloc())) then
	return
end

function UnitNetworkHandler._verify_sender(_ARG_0_, ...)
	if not _ARG_0_ then
		return nil
	end

	return UnitNetworkHandler.oldMethods._verify_sender(_ARG_0_, ...)
end

function UnitNetworkHandler.alarm_pager_interaction(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_)
	if not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_ingame) then
		return
	end

	if _ARG_3_ == 3 then
		managers.hud:set_control_info({pager_answered = true})
	end

	return _ARG_0_.oldMethods.alarm_pager_interaction(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_)
end

function UnitNetworkHandler._is_civilian(_ARG_0_, _ARG_1_)
	return alive(_ARG_1_) and managers.enemy:is_civilian(_ARG_1_) or false
end

function UnitNetworkHandler._record_ai_death(_ARG_0_, _ARG_1_, _ARG_2_)
	if not alive(_ARG_1_) or _ARG_0_:_is_civilian(_ARG_1_) or not _ARG_2_ or not _ARG_2_.network then
		return
	end

	if managers.hud:teammate_panel_from_peer_id(_ARG_2_:network():peer():id()) then
		managers.hud:increment_kill_counter((managers.hud:teammate_panel_from_peer_id(_ARG_2_:network():peer():id())))
	end
end

function UnitNetworkHandler.damage_bullet(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_)
	if not _ARG_0_._verify_character_and_sender(_ARG_1_, _ARG_7_) or not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_ingame) then
		return _ARG_0_.oldMethods.damage_bullet(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_)
	end

	if _ARG_6_ then
		pcall(_ARG_0_._record_ai_death, _ARG_0_, _ARG_1_, _ARG_2_)
	end

	return _ARG_0_.oldMethods.damage_bullet(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_)
end

function UnitNetworkHandler.damage_explosion(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_)
	if not _ARG_0_._verify_character_and_sender(_ARG_1_, _ARG_7_) or not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_ingame) then
		return _ARG_0_.oldMethods.damage_explosion(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_)
	end

	if _ARG_5_ then
		pcall(_ARG_0_._record_ai_death, _ARG_0_, _ARG_1_, _ARG_2_)
	end

	return _ARG_0_.oldMethods.damage_explosion(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_)
end

function UnitNetworkHandler.damage_melee(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_, _ARG_8_, _ARG_9_)
	if not _ARG_0_._verify_character_and_sender(_ARG_1_, _ARG_9_) or not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_ingame) then
		return _ARG_0_.oldMethods.damage_melee(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_, _ARG_8_, _ARG_9_)
	end

	if _ARG_8_ then
		pcall(_ARG_0_._record_ai_death, _ARG_0_, _ARG_1_, _ARG_2_)
	end

	return _ARG_0_.oldMethods.damage_melee(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_, _ARG_8_, _ARG_9_)
end

function UnitNetworkHandler.sync_player_movement_state(_ARG_0_, _ARG_1_, ...)
	if not alive(_ARG_1_) or not _ARG_1_:movement() then
		return
	end

	return _ARG_0_.oldMethods.sync_player_movement_state(_ARG_0_, _ARG_1_, ...)
end

function UnitNetworkHandler.check_loot_cheater(_ARG_0_, _ARG_1_, _ARG_2_)
	if tweak_data.hoxhud.cheater_check_disabled or _ARG_2_ < 29000 then
		return
	end

	_ARG_0_._hoxhud_tbl._secure_count[_ARG_0_._verify_sender(_ARG_1_):user_id()] = _ARG_0_._hoxhud_tbl._secure_count[_ARG_0_._verify_sender(_ARG_1_):user_id()] and _ARG_0_._hoxhud_tbl._secure_count[_ARG_0_._verify_sender(_ARG_1_):user_id()] + 1 or 1
	if not Network:is_server() or not (_ARG_0_._hoxhud_tbl._secure_count[_ARG_0_._verify_sender(_ARG_1_):user_id()] > 1) then
	elseif _ARG_0_._hoxhud_tbl._secure_count[_ARG_0_._verify_sender(_ARG_1_):user_id()] > (tweak_data.hoxhud.cheater_secure_limit[Global.game_settings.level_id] or tweak_data.hoxhud.cheater_default_secure_limit) then
		if Network:is_server() then
			managers.hud:set_cheater_teammate(managers.hud:teammate_panel_from_peer_id((_ARG_0_._verify_sender(_ARG_1_):user_id())))
		else
		end

		managers.hud._hud.present_queue = {}
		managers.hud:present_mid_text({
			title = "CHEATER!",
			text = "Someone is securing too many bags!",
			time = 3
		})
	end
end

function UnitNetworkHandler.sync_secure_loot(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	if tweak_data.hoxhud.HUD_ONLY then
		return _ARG_0_.oldMethods.sync_secure_loot(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	end

	if not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_ingame) and not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_end_game) or not _ARG_0_._verify_sender({
		...
	}[#{
		...
	}]) then
		return
	end

	_ARG_0_:check_loot_cheater({
		...
	}[#{
		...
	}], tweak_data.carry.small_loot[_ARG_1_] and _ARG_2_ or 1000000)
	return _ARG_0_.oldMethods.sync_secure_loot(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
end

function UnitNetworkHandler.server_secure_loot(_ARG_0_, _ARG_1_, _ARG_2_, ...)
	if tweak_data.hoxhud.HUD_ONLY then
		return _ARG_0_.oldMethods.server_secure_loot(_ARG_0_, _ARG_1_, _ARG_2_, ...)
	end

	if not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_ingame) or not _ARG_0_._verify_sender({
		...
	}[#{
		...
	}]) then
		return
	end

	_ARG_0_:check_loot_cheater({
		...
	}[#{
		...
	}], 1000000)
	if _ARG_0_:get_carry_bag(_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id()) and not _ARG_0_:is_carry_blacklisted(_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id()) then
		return _ARG_0_.oldMethods.server_secure_loot(_ARG_0_, _ARG_1_, _ARG_2_, ...)
	end
end

function UnitNetworkHandler.update_carry_bag(_ARG_0_, _ARG_1_, _ARG_2_)
	_ARG_0_._hoxhud_tbl._iscarrying[_ARG_1_] = _ARG_2_
end

function UnitNetworkHandler.get_carry_bag(_ARG_0_, _ARG_1_)
	return _ARG_0_._hoxhud_tbl._iscarrying[_ARG_1_] and _ARG_0_._hoxhud_tbl._iscarrying[_ARG_1_].tbl or nil
end

function UnitNetworkHandler.is_bag_drop_correct(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_)
	if not _ARG_0_._hoxhud_tbl._iscarrying[_ARG_1_] then
		return false
	end

	return _ARG_0_._hoxhud_tbl._iscarrying[_ARG_1_].id == _ARG_2_ and _ARG_0_._hoxhud_tbl._iscarrying[_ARG_1_].value == _ARG_3_
end

function UnitNetworkHandler.update_carry_allowed(_ARG_0_, _ARG_1_, _ARG_2_)
	_ARG_0_._hoxhud_tbl._carryallowed[_ARG_1_] = _ARG_2_
end

function UnitNetworkHandler.is_carry_allowed(_ARG_0_, _ARG_1_)
	return _ARG_0_._hoxhud_tbl._carryallowed[_ARG_1_]
end

function UnitNetworkHandler.set_carry_blacklisted(_ARG_0_, _ARG_1_, _ARG_2_)
	_ARG_0_._hoxhud_tbl._carryblacklist[_ARG_1_] = _ARG_2_
end

function UnitNetworkHandler.is_carry_blacklisted(_ARG_0_, _ARG_1_)
	return _ARG_0_._hoxhud_tbl._carryblacklist[_ARG_1_]
end

function UnitNetworkHandler.set_carry_interact_time(_ARG_0_, _ARG_1_, _ARG_2_)
	_ARG_0_._hoxhud_tbl._carryInteractTime[_ARG_1_] = _ARG_2_
end

function UnitNetworkHandler.get_carry_interact_time(_ARG_0_, _ARG_1_)
	return _ARG_0_._hoxhud_tbl._carryInteractTime[_ARG_1_] and _ARG_0_._hoxhud_tbl._carryInteractTime[_ARG_1_] or Application:time()
end

function UnitNetworkHandler.sync_carry(_ARG_0_, ...)
	if tweak_data.hoxhud.HUD_ONLY then
		return _ARG_0_.oldMethods.sync_carry(_ARG_0_, ...)
	end

	if #{
		...
	} ~= 6 or not _ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):id() then
	end

	if not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_ingame) or not _ARG_0_._verify_sender({
		...
	}[#{
		...
	}]) or table.remove({
		...
	}, 1) ~= _ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):id() and (not Network:is_client() or _ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):id() ~= 1) then
		return
	end

	if Network:is_server() and not tweak_data.hoxhud.disable_carry_bag_check then
		if not _ARG_0_:get_carry_bag((_ARG_0_._verify_sender({
			...
		}[#{
			...
		}]):user_id())) and not _ARG_0_:is_carry_blacklisted((_ARG_0_._verify_sender({
			...
		}[#{
			...
		}]):user_id())) then
			_ARG_0_:update_carry_bag(_ARG_0_._verify_sender({
				...
			}[#{
				...
			}]):user_id(), {
				id = unpack({
					...
				})
			})
		else
			_ARG_0_:set_carry_blacklisted(_ARG_0_._verify_sender({
				...
			}[#{
				...
			}]):user_id(), true)
		end
	end

	_ARG_0_.oldMethods.sync_carry(_ARG_0_, ...)
end

function UnitNetworkHandler.sync_remove_carry(_ARG_0_, ...)
	if tweak_data.hoxhud.HUD_ONLY then
		return _ARG_0_.oldMethods.sync_remove_carry(_ARG_0_, ...)
	end

	if not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_ingame) or not _ARG_0_._verify_sender({
		...
	}[#{
		...
	}]) then
		return
	end

	if not tweak_data.hoxhud.disable_carry_bag_check and _ARG_0_:is_carry_allowed((_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id())) or (tweak_data.hoxhud.bag_interact_cutoff or 1.2) < Application:time() - _ARG_0_:get_carry_interact_time((_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id())) or _ARG_0_:is_carry_blacklisted((_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id())) then
		_ARG_0_:update_carry_allowed(_ARG_0_._verify_sender({
			...
		}[#{
			...
		}]):user_id(), false)
		if type(_ARG_0_:get_carry_bag((_ARG_0_._verify_sender({
			...
		}[#{
			...
		}]):user_id()))) == "table" then
			_ARG_0_:set_carry_blacklisted(_ARG_0_._verify_sender({
				...
			}[#{
				...
			}]):user_id(), true)
			managers.player:set_anticheat_override(true)
			managers.player:server_drop_carry(unpack(_ARG_0_:get_carry_bag((_ARG_0_._verify_sender({
				...
			}[#{
				...
			}]):user_id()))))
			managers.player:set_anticheat_override(nil)
		end
	end

	_ARG_0_:update_carry_allowed(_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id(), true)
	_ARG_0_:update_carry_bag(_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id(), nil)
	_ARG_0_.oldMethods.sync_remove_carry(_ARG_0_, ...)
end

function UnitNetworkHandler.server_drop_carry(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_, _ARG_8_, _ARG_9_, ...)
	if tweak_data.hoxhud.HUD_ONLY then
		return _ARG_0_.oldMethods.server_drop_carry(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_, _ARG_8_, _ARG_9_, ...)
	end

	if not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_ingame) or not _ARG_0_._verify_sender({
		...
	}[#{
		...
	}]) then
		return
	end

	if tweak_data.hoxhud.disable_carry_bag_check or not _ARG_0_:is_carry_allowed((_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id())) and not _ARG_0_:is_carry_blacklisted((_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id())) and _ARG_0_:is_bag_drop_correct(_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id(), _ARG_1_, _ARG_2_) then
		_ARG_0_:update_carry_bag(_ARG_0_._verify_sender({
			...
		}[#{
			...
		}]):user_id(), nil)
		return _ARG_0_.oldMethods.server_drop_carry(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_, _ARG_8_, _ARG_9_, ...)
	end

	managers.hud:set_cheater_teammate(managers.hud:teammate_panel_from_peer_id(_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):id()))
	managers.hud._hud.present_queue = {}
	managers.hud:present_mid_text({
		title = "CHEATER!",
		text = _ARG_0_._verify_sender({
			...
		}[#{
			...
		}]):name() .. " is trying to spawn in lootbags.",
		time = 3
	})
	_ARG_0_:update_carry_allowed(_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id(), false)
	_ARG_0_:update_carry_bag(_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id(), nil)
	_ARG_0_:set_carry_blacklisted(_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id(), true)
end

function UnitNetworkHandler.is_carry_interaction(_ARG_0_, _ARG_1_)
	if tweak_data.hoxhud.carry_interactions[_ARG_1_] then
		return true
	end

	return tweak_data.interaction[_ARG_1_] and tweak_data.interaction[_ARG_1_].blocked_hint and tweak_data.interaction[_ARG_1_].blocked_hint == "carry_block" and true or false
end

function UnitNetworkHandler.sync_interacted(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, ...)
	if tweak_data.hoxhud.HUD_ONLY then
		return _ARG_0_.oldMethods.sync_interacted(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, ...)
	end

	if not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_ingame) or not _ARG_0_._verify_sender({
		...
	}[#{
		...
	}]) then
		return
	end

	_ARG_0_._hoxhud_tbl._peerinteract[_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id()] = Application:time()
	if Network:is_server() then
		if _ARG_0_:is_carry_interaction(_ARG_3_) and _ARG_0_:get_carry_bag((_ARG_0_._verify_sender({
			...
		}[#{
			...
		}]):user_id())) and not _ARG_0_:is_carry_blacklisted((_ARG_0_._verify_sender({
			...
		}[#{
			...
		}]):user_id())) then
			_ARG_0_:update_carry_allowed(_ARG_0_._verify_sender({
				...
			}[#{
				...
			}]):user_id(), false)
			_ARG_0_:set_carry_interact_time(_ARG_0_._verify_sender({
				...
			}[#{
				...
			}]):user_id(), Application:time())
		elseif Application:time() - (_ARG_0_._hoxhud_tbl._peerinteract[_ARG_0_._verify_sender({
			...
		}[#{
			...
		}]):user_id()] or Application:time() - 3) < 0.05 or _ARG_0_:is_carry_interaction(_ARG_3_) then
			if _ARG_0_:is_carry_interaction(_ARG_3_) then
				_ARG_0_:set_carry_blacklisted(_ARG_0_._verify_sender({
					...
				}[#{
					...
				}]):user_id(), true)
				if type(_ARG_0_:get_carry_bag((_ARG_0_._verify_sender({
					...
				}[#{
					...
				}]):user_id()))) == "table" then
					_ARG_0_:update_carry_bag(_ARG_0_._verify_sender({
						...
					}[#{
						...
					}]):user_id(), nil)
					managers.player:server_drop_carry(unpack(_ARG_0_:get_carry_bag((_ARG_0_._verify_sender({
						...
					}[#{
						...
					}]):user_id()))))
				end

			end

			if _ARG_0_:is_carry_interaction(_ARG_3_) and not tweak_data.hoxhud.disable_carry_bag_check or not _ARG_0_:is_carry_interaction(_ARG_3_) and not tweak_data.hoxhud.disable_interaction_cheat_checks then
				{
					...
				}[#{
					...
				}]:sync_interaction_reply(false)
				if alive(_ARG_1_) then
					_ARG_1_:interaction():set_active(true, true, false)
				end

				return
			end

		end
	end

	if _ARG_3_ == "corpse_alarm_pager" and _ARG_4_ == 3 then
		managers.hud:set_control_info({pager_answered = true})
	end

	_ARG_0_.oldMethods.sync_interacted(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, ...)
end

function UnitNetworkHandler.sync_grenades(_ARG_0_, ...)
	if tweak_data.hoxhud.HUD_ONLY then
		return _ARG_0_.oldMethods.sync_grenades(_ARG_0_, ...)
	end

	if not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_ingame) or not _ARG_0_._verify_sender({
		...
	}[#{
		...
	}]) then
		return
	end

	_ARG_0_.oldMethods.sync_grenades(_ARG_0_, ...)
	_ARG_0_._hoxhud_tbl._grenadecounter[_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id()] = _ARG_0_._hoxhud_tbl._grenadecounter[_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id()] or tweak_data.upgrades.max_grenade_amount
	if (#{
		...
	} == 3 and {
		...
	}[2] or {
		...
	}[3]) - _ARG_0_._hoxhud_tbl._grenadecounter[_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id()] == 1 and (#{
		...
	} == 3 and {
		...
	}[2] or {
		...
	}[3]) < 4 then
		_ARG_0_:give_player_additional_grenade((_ARG_0_._verify_sender({
			...
		}[#{
			...
		}]):user_id()))
	end
end

function UnitNetworkHandler.give_player_additional_grenade(_ARG_0_, _ARG_1_)
	_ARG_0_._hoxhud_tbl._grenadestaken[_ARG_1_] = _ARG_0_._hoxhud_tbl._grenadestaken[_ARG_1_] or 0
	if _ARG_0_._hoxhud_tbl._grenadestaken[_ARG_1_] < 3 then
		_ARG_0_._hoxhud_tbl._grenadecounter[_ARG_1_] = _ARG_0_._hoxhud_tbl._grenadecounter[_ARG_1_] + 1 or 1
		_ARG_0_._hoxhud_tbl._grenadestaken[_ARG_1_] = _ARG_0_._hoxhud_tbl._grenadestaken[_ARG_1_] + 1
	end
end

function UnitNetworkHandler.server_throw_grenade(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	if tweak_data.hoxhud.HUD_ONLY then
		return _ARG_0_.oldMethods.server_throw_grenade(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	end

	if not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_ingame) or not _ARG_0_._verify_sender({
		...
	}[#{
		...
	}]) then
		return
	end

	_ARG_0_._hoxhud_tbl._grenadecounter[_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id()] = _ARG_0_._hoxhud_tbl._grenadecounter[_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id()] or tweak_data.upgrades.max_grenade_amount
	if _ARG_0_._hoxhud_tbl._grenadecounter[_ARG_0_._verify_sender({
		...
	}[#{
		...
	}]):user_id()] > 0 and (not managers.groupai:state():whisper_mode() or tweak_data.hoxhud.allow_grenades_in_stealth) then
		_ARG_0_.oldMethods.server_throw_grenade(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
		_ARG_0_._hoxhud_tbl._grenadecounter[_ARG_0_._verify_sender({
			...
		}[#{
			...
		}]):user_id()] = _ARG_0_._hoxhud_tbl._grenadecounter[_ARG_0_._verify_sender({
			...
		}[#{
			...
		}]):user_id()] - 1
	end
end

function UnitNetworkHandler.check_peer_deployable(_ARG_0_, _ARG_1_, _ARG_2_)
	_ARG_0_._hoxhud_tbl._peer_deployable[_ARG_1_] = _ARG_0_._hoxhud_tbl._peer_deployable[_ARG_1_] or _ARG_2_
	return _ARG_0_._hoxhud_tbl._peer_deployable[_ARG_1_] == _ARG_2_
end

function UnitNetworkHandler.place_deployable_bag(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, ...)
	if tweak_data.hoxhud.HUD_ONLY then
		return _ARG_0_.oldMethods.place_deployable_bag(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, ...)
	end

	if not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_ingame) or not _ARG_0_._verify_sender(_ARG_5_) or not _ARG_0_:check_peer_deployable(_ARG_0_._verify_sender(_ARG_5_):user_id(), _ARG_1_) then
		return
	end

	_ARG_0_._hoxhud_tbl._deployablecounter[_ARG_0_._verify_sender(_ARG_5_):user_id()] = _ARG_0_._hoxhud_tbl._deployablecounter[_ARG_0_._verify_sender(_ARG_5_):user_id()] or tweak_data.hoxhud.max_deployables
	if _ARG_0_._hoxhud_tbl._deployablecounter[_ARG_0_._verify_sender(_ARG_5_):user_id()] > 0 then
		_ARG_0_.oldMethods.place_deployable_bag(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, ...)
		_ARG_0_._hoxhud_tbl._deployablecounter[_ARG_0_._verify_sender(_ARG_5_):user_id()] = _ARG_0_._hoxhud_tbl._deployablecounter[_ARG_0_._verify_sender(_ARG_5_):user_id()] - 1
	end
end

function UnitNetworkHandler.place_sentry_gun(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_, _ARG_8_, ...)
	if tweak_data.hoxhud.HUD_ONLY then
		return _ARG_0_.oldMethods.place_sentry_gun(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_, _ARG_8_, ...)
	end

	if not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_ingame) or not _ARG_0_._verify_sender(_ARG_8_) or not _ARG_0_:check_peer_deployable(_ARG_0_._verify_sender(_ARG_8_):user_id(), "sentry") then
		return
	end

	_ARG_0_._hoxhud_tbl._sentrycounter[_ARG_0_._verify_sender(_ARG_8_):user_id()] = _ARG_0_._hoxhud_tbl._sentrycounter[_ARG_0_._verify_sender(_ARG_8_):user_id()] or tweak_data.hoxhud.max_player_sentries
	if _ARG_0_._hoxhud_tbl._sentrycounter[_ARG_0_._verify_sender(_ARG_8_):user_id()] > 0 then
		_ARG_0_.oldMethods.place_sentry_gun(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_, _ARG_8_, ...)
		_ARG_0_._hoxhud_tbl._sentrycounter[_ARG_0_._verify_sender(_ARG_8_):user_id()] = _ARG_0_._hoxhud_tbl._sentrycounter[_ARG_0_._verify_sender(_ARG_8_):user_id()] - 1
	end
end

function UnitNetworkHandler.attach_device(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, ...)
	if tweak_data.hoxhud.HUD_ONLY then
		return _ARG_0_.oldMethods.attach_device(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, ...)
	end

	if not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_ingame) or not _ARG_0_._verify_sender(_ARG_4_) or not _ARG_0_:check_peer_deployable(_ARG_0_._verify_sender(_ARG_4_):user_id(), "tripmine") then
		return
	end

	_ARG_0_._hoxhud_tbl._tripminecounter[_ARG_0_._verify_sender(_ARG_4_):user_id()] = _ARG_0_._hoxhud_tbl._tripminecounter[_ARG_0_._verify_sender(_ARG_4_):user_id()] or tweak_data.hoxhud.max_player_tripmines
	if _ARG_0_._hoxhud_tbl._tripminecounter[_ARG_0_._verify_sender(_ARG_4_):user_id()] > 0 then
		_ARG_0_.oldMethods.attach_device(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, ...)
		_ARG_0_._hoxhud_tbl._tripminecounter[_ARG_0_._verify_sender(_ARG_4_):user_id()] = _ARG_0_._hoxhud_tbl._tripminecounter[_ARG_0_._verify_sender(_ARG_4_):user_id()] - 1
	end
end

function UnitNetworkHandler.request_place_ecm_jammer(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, ...)
	if tweak_data.hoxhud.HUD_ONLY then
		return _ARG_0_.oldMethods.request_place_ecm_jammer(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, ...)
	end

	if not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_ingame) or not _ARG_0_._verify_sender(_ARG_4_) or not _ARG_0_:check_peer_deployable(_ARG_0_._verify_sender(_ARG_4_):user_id(), "ecm") then
		return
	end

	_ARG_0_._hoxhud_tbl._ecmcounter[_ARG_0_._verify_sender(_ARG_4_):user_id()] = _ARG_0_._hoxhud_tbl._ecmcounter[_ARG_0_._verify_sender(_ARG_4_):user_id()] or tweak_data.hoxhud.max_player_ecms
	if _ARG_0_._hoxhud_tbl._ecmcounter[_ARG_0_._verify_sender(_ARG_4_):user_id()] > 0 then
		_ARG_0_.oldMethods.request_place_ecm_jammer(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, ...)
		_ARG_0_._hoxhud_tbl._ecmcounter[_ARG_0_._verify_sender(_ARG_4_):user_id()] = _ARG_0_._hoxhud_tbl._ecmcounter[_ARG_0_._verify_sender(_ARG_4_):user_id()] - 1
	end
end

function UnitNetworkHandler.from_server_ecm_jammer_placed(_ARG_0_, _ARG_1_, ...)
	if Network:is_client() and UnitNetworkHandler._requestedECM then
		UnitNetworkHandler._requestedECM = nil
		_ARG_1_:base()._max_battery_life = tweak_data.upgrades.ecm_jammer_base_battery_life * (managers.player:upgrade_value("ecm_jammer", "duration_multiplier", 1) * managers.player:upgrade_value("ecm_jammer", "duration_multiplier_2", 1))
		_ARG_1_:base()._battery_life = _ARG_1_:base()._max_battery_life
	end

	return _ARG_0_.oldMethods.from_server_ecm_jammer_placed(_ARG_0_, _ARG_1_, ...)
end

function UnitNetworkHandler.from_server_ecm_jammer_rejected(_ARG_0_, ...)
	UnitNetworkHandler._requestedECM = nil
	return _ARG_0_.oldMethods.from_server_ecm_jammer_rejected(_ARG_0_, ...)
end

function UnitNetworkHandler.sync_player_movement_state(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, ...)
	if not _ARG_0_._verify_gamestate(_ARG_0_._gamestate_filter.any_ingame) or not alive(_ARG_1_) or not _ARG_2_ or not _ARG_3_ or not _ARG_4_ then
		return
	end

	if _ARG_1_ == managers.player:player_unit() then
		managers.player:player_unit():network():send("sync_player_movement_state", _ARG_1_:movement()._current_state_name, _ARG_1_:character_damage():down_time(), _ARG_1_:id())
		return
	end

	return _ARG_0_.oldMethods.sync_player_movement_state(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, ...)
end

function UnitNetworkHandler.set_equipped_weapon(_ARG_0_, _ARG_1_, _ARG_2_, ...)
	if not _ARG_0_._verify_character_and_sender(_ARG_1_, {
		...
	}[#{
		...
	}]) or _ARG_1_ == managers.player:player_unit() or not HuskPlayerInventory._index_to_weapon_list[_ARG_2_] then
		return
	end

	_ARG_0_.oldMethods.set_equipped_weapon(_ARG_0_, _ARG_1_, _ARG_2_, ...)
end

function UnitNetworkHandler.sync_show_hint(_ARG_0_, _ARG_1_, ...)
	if managers.hint:hint(_ARG_1_) then
		return _ARG_0_.oldMethods.sync_show_hint(_ARG_0_, _ARG_1_, ...)
	end
end

