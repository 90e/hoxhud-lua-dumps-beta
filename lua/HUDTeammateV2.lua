clone_methods(HUDTeammate)
function HUDTeammate.set_stamina_value(_ARG_0_, _ARG_1_)
	if _ARG_0_._stamina then
		_ARG_0_._stamina:set_current(_ARG_1_ / _ARG_0_._max_stamina)
	end
end

function HUDTeammate.set_max_stamina(_ARG_0_, _ARG_1_)
	_ARG_0_._max_stamina = _ARG_1_
end

function HUDTeammate.inject_revivecounter(_ARG_0_)
	_ARG_0_._revives_counter = _ARG_0_._player_panel:child("radial_health_panel"):text({
		name = "revives_counter",
		visible = not tweak_data.hoxhud.ANTICHEAT_ONLY,
		text = "0",
		layer = 4,
		color = Color.white,
		w = _ARG_0_._player_panel:child("radial_health_panel"):w(),
		x = 0,
		y = 0,
		h = _ARG_0_._player_panel:child("radial_health_panel"):h(),
		vertical = "center",
		align = "center",
		font_size = 20,
		font = tweak_data.hud_players.ammo_font
	})
	_ARG_0_._revive_colors = tweak_data.hoxhud.revive_counter_colors
	_ARG_0_._revives_count = 0
end

function HUDTeammate.increment_revives(_ARG_0_)
	if _ARG_0_._revives_counter then
		_ARG_0_._revives_count = _ARG_0_._revives_count + 1
		_ARG_0_._revives_counter:set_text(tostring(_ARG_0_._revives_count))
	end
end

function HUDTeammate.reset_revives(_ARG_0_)
	if _ARG_0_._revives_counter then
		_ARG_0_._revives_count = 0
		_ARG_0_._revives_counter:set_text(tostring(_ARG_0_._revives_count))
	end
end

function HUDTeammate.set_health(_ARG_0_, _ARG_1_)
	if not _ARG_0_._main_player and _ARG_0_._player_panel:child("radial_health_panel"):child("radial_health"):color().r < 1 and 1 <= _ARG_1_.current / _ARG_1_.total then
		_ARG_0_:reset_revives()
	end

	if not _ARG_1_.revives or tweak_data.hoxhud.ANTICHEAT_ONLY then
		return _ARG_0_.oldMethods.set_health(_ARG_0_, _ARG_1_)
	end

	_ARG_0_._revives_counter:set_color(_ARG_0_._revive_colors[_ARG_1_.revives - 1] or Color.black:with_alpha(0.9))
	if not (_ARG_0_._main_player and (managers.player:player_unit() and managers.player:player_unit():character_damage() or {}._messiah_charges or managers.player:upgrade_value("player", "pistol_revive_from_bleed_out", 0)) or nil) or not (0 < (_ARG_0_._main_player and (managers.player:player_unit() and managers.player:player_unit():character_damage() or {}._messiah_charges or managers.player:upgrade_value("player", "pistol_revive_from_bleed_out", 0)) or nil)) or not (_ARG_1_.revives - 1 .. "/" .. (_ARG_0_._main_player and (managers.player:player_unit() and managers.player:player_unit():character_damage() or {}._messiah_charges or managers.player:upgrade_value("player", "pistol_revive_from_bleed_out", 0)) or nil)) then
	end

	_ARG_0_._revives_counter:set_text((tostring(_ARG_1_.revives - 1)))
	return _ARG_0_.oldMethods.set_health(_ARG_0_, _ARG_1_)
end

function HUDTeammate.set_callsign(_ARG_0_, _ARG_1_, ...)
	_ARG_0_.oldMethods.set_callsign(_ARG_0_, _ARG_1_, ...)
	if tweak_data.hoxhud.ANTICHEAT_ONLY then
		return
	end

	_ARG_0_._panel:child("callsign_ptt"):set_color(_ARG_0_._panel:child("callsign"):color())
	if tweak_data.hoxhud.set_name_text_color_to_callsign then
		_ARG_0_._panel:child("name"):set_color(_ARG_0_._panel:child("callsign"):color():with_alpha(1))
	end
end

function HUDTeammate.set_player_is_talking(_ARG_0_, _ARG_1_)
	_ARG_0_._panel:child("callsign"):set_visible(not _ARG_1_)
	_ARG_0_._panel:child("callsign_ptt"):set_visible(_ARG_1_)
end

function HUDTeammate.inject_stamina(_ARG_0_)
	_ARG_0_._player_panel:panel({
		name = "stamina_panel",
		visible = not tweak_data.hoxhud.ANTICHEAT_ONLY,
		layer = 0
	}):set_shape(_ARG_0_._player_panel:child("radial_health_panel"):shape())
	_ARG_0_._player_panel:panel({
		name = "stamina_panel",
		visible = not tweak_data.hoxhud.ANTICHEAT_ONLY,
		layer = 0
	}):set_size(36.48, 36.48)
	_ARG_0_._player_panel:panel({
		name = "stamina_panel",
		visible = not tweak_data.hoxhud.ANTICHEAT_ONLY,
		layer = 0
	}):set_center(_ARG_0_._player_panel:child("radial_health_panel"):center())
	_ARG_0_._stamina = CircleBitmapGuiObject:new(_ARG_0_._player_panel:panel({
		name = "stamina_panel",
		visible = not tweak_data.hoxhud.ANTICHEAT_ONLY,
		layer = 0
	}), {
		use_bg = false,
		rotation = 360,
		radius = _ARG_0_._player_panel:panel({
			name = "stamina_panel",
			visible = not tweak_data.hoxhud.ANTICHEAT_ONLY,
			layer = 0
		}):h() / 2 - 4,
		blend_mode = "normal",
		layer = 1,
		alpha = 1
	})
	_ARG_0_._stamina:set_image("guis/textures/pd2/hud_radial_rim")
	_ARG_0_._stamina:set_position(4, 4)
	_ARG_0_._stamina:set_color(Color.red)
end

function HUDTeammate.inject_health_glow(_ARG_0_)
	_ARG_0_._radial_health_glow = _ARG_0_._player_panel:child("radial_health_panel"):panel({
		visible = not tweak_data.hoxhud.ANTICHEAT_ONLY,
		name = "health_glow_panel",
		h = _ARG_0_._player_panel:child("radial_health_panel"):h(),
		w = _ARG_0_._player_panel:child("radial_health_panel"):w(),
		layer = 0
	}):bitmap({
		name = "glow",
		texture = "guis/textures/pd2/crimenet_marker_glow",
		texture_rect = {
			1,
			1,
			62,
			62
		},
		h = _ARG_0_._player_panel:child("radial_health_panel"):h() - 0,
		w = _ARG_0_._player_panel:child("radial_health_panel"):w() - 0,
		color = tweak_data.hoxhud.health_damage_dampen_glow_color,
		blend_mode = "add",
		layer = 2,
		align = "center",
		visible = false,
		rotation = 360
	})
	_ARG_0_._radial_health_glow:set_position(0 / 2, 0 / 2)
end

function HUDTeammate.inject_killcounter(_ARG_0_)
	_ARG_0_._panel:bitmap({
		name = "kill_icon",
		texture = "guis/textures/pd2/crimenet_skull",
		x = _ARG_0_._panel:child("name"):right(),
		color = Color.yellow,
		y = 0,
		vertical = "center",
		valign = "center",
		h = _ARG_0_._panel:child("name"):text_rect()
	}):set_leftbottom(_ARG_0_._panel:child("name"):text_rect())
	_ARG_0_._kill_counter = _ARG_0_._panel:text({
		name = "kill_counter",
		text = "0",
		layer = 1,
		color = tweak_data.hoxhud.kill_counter_color,
		x = _ARG_0_._panel:child("name"):right() + _ARG_0_._panel:bitmap({
			name = "kill_icon",
			texture = "guis/textures/pd2/crimenet_skull",
			x = _ARG_0_._panel:child("name"):right(),
			color = Color.yellow,
			y = 0,
			vertical = "center",
			valign = "center",
			h = _ARG_0_._panel:child("name"):text_rect()
		}):w(),
		y = -1,
		vertical = "bottom",
		font_size = tweak_data.hud_players.name_size,
		font = tweak_data.hud_players.name_font
	})
	_ARG_0_._kill_counter:set_leftbottom(_ARG_0_._kill_counter:h(), _ARG_0_._panel:h() - 70)
	_ARG_0_._kill_counter_bg = _ARG_0_._panel:bitmap({
		name = "kill_counter_bg",
		texture = "guis/textures/pd2/hud_tabs",
		texture_rect = {
			84,
			0,
			44,
			32
		},
		layer = 0,
		color = Color.white / 3,
		h = _ARG_0_._kill_counter:text_rect()
	})
	_ARG_0_._kill_counter_bg:set_w(_ARG_0_._kill_counter:text_rect() + 4)
	_ARG_0_._kills = 0
end

function HUDTeammate.inject_armor_regen_timer(_ARG_0_)
	_ARG_0_._armor_regen = _ARG_0_._player_panel:text({
		name = "armor_regen",
		text = "0.0s",
		color = tweak_data.hoxhud.armor_regen_color or Color.white,
		visible = false,
		align = "left",
		vertical = "bottom",
		font = tweak_data.hud_players.name_font,
		font_size = tweak_data.hoxhud.armor_regen_font_size,
		layer = 4
	})
	_ARG_0_._armor_regenbg1 = _ARG_0_._player_panel:text({
		name = "armor_regenbg1",
		text = "0.0s",
		color = Color.black:with_alpha(tweak_data.hoxhud.armor_regen_color or Color.white.a),
		visible = false,
		align = "left",
		vertical = "bottom",
		font = tweak_data.hud_players.name_font,
		font_size = tweak_data.hoxhud.armor_regen_font_size,
		layer = 3
	})
	_ARG_0_._armor_regenbg2 = _ARG_0_._player_panel:text({
		name = "armor_regenbg2",
		text = "0.0s",
		color = Color.black:with_alpha(tweak_data.hoxhud.armor_regen_color or Color.white.a),
		visible = false,
		align = "left",
		vertical = "bottom",
		font = tweak_data.hud_players.name_font,
		font_size = tweak_data.hoxhud.armor_regen_font_size,
		layer = 3
	})
	_ARG_0_._armor_regenbg3 = _ARG_0_._player_panel:text({
		name = "armor_regenbg3",
		text = "0.0s",
		color = Color.black:with_alpha(tweak_data.hoxhud.armor_regen_color or Color.white.a),
		visible = false,
		align = "left",
		vertical = "bottom",
		font = tweak_data.hud_players.name_font,
		font_size = tweak_data.hoxhud.armor_regen_font_size,
		layer = 3
	})
	_ARG_0_._armor_regenbg4 = _ARG_0_._player_panel:text({
		name = "armor_regenbg4",
		text = "0.0s",
		color = Color.black:with_alpha(tweak_data.hoxhud.armor_regen_color or Color.white.a),
		visible = false,
		align = "left",
		vertical = "bottom",
		font = tweak_data.hud_players.name_font,
		font_size = tweak_data.hoxhud.armor_regen_font_size,
		layer = 3
	})
	_ARG_0_._armor_regenbg1:set_x(_ARG_0_._armor_regenbg1:x() - 1)
	_ARG_0_._armor_regenbg1:set_y(_ARG_0_._armor_regenbg1:y() - 1)
	_ARG_0_._armor_regenbg2:set_x(_ARG_0_._armor_regenbg2:x() + 1)
	_ARG_0_._armor_regenbg2:set_y(_ARG_0_._armor_regenbg2:y() - 1)
	_ARG_0_._armor_regenbg3:set_x(_ARG_0_._armor_regenbg3:x() - 1)
	_ARG_0_._armor_regenbg3:set_y(_ARG_0_._armor_regenbg3:y() + 1)
	_ARG_0_._armor_regenbg4:set_x(_ARG_0_._armor_regenbg4:x() + 1)
	_ARG_0_._armor_regenbg4:set_y(_ARG_0_._armor_regenbg4:y() + 1)
end

function HUDTeammate.inject_interact_info(_ARG_0_)
	_ARG_0_._interact_info_panel = _ARG_0_._panel:panel({
		name = "interact_info_panel",
		x = 0,
		y = 0,
		visible = false
	})
	_ARG_0_._interact_info = _ARG_0_._interact_info_panel:text({
		name = "interact_info",
		text = "|",
		layer = 3,
		color = Color.white,
		x = 0,
		y = 1,
		align = "right",
		vertical = "top",
		font_size = tweak_data.hud_players.name_size,
		font = tweak_data.hud_players.name_font
	})
	_ARG_0_._interact_info:set_right(_ARG_0_._interact_info_panel:w() - 8)
	_ARG_0_._interact_info_bg = _ARG_0_._interact_info_panel:bitmap({
		name = "interact_info_bg",
		texture = "guis/textures/pd2/hud_tabs",
		texture_rect = {
			84,
			0,
			44,
			32
		},
		layer = 2,
		color = Color.white / 3,
		x = 0,
		y = 0,
		align = "right",
		w = _ARG_0_._interact_info:text_rect() + 4,
		h = _ARG_0_._interact_info:text_rect()
	})
end

function HUDTeammate.inject_infinite_ammo_glow(_ARG_0_)
	_ARG_0_._ammo_is_infinite = false
	_ARG_0_._primary_inf_glow = _ARG_0_._player_panel:child("weapons_panel"):child("primary_weapon_panel"):bitmap({
		name = "glow",
		texture = "guis/textures/pd2/crimenet_marker_glow",
		h = 64,
		w = 64,
		color = tweak_data.hoxhud.infinite_ammo_glow_color,
		blend_mode = "add",
		layer = 2,
		align = "center",
		visible = false,
		rotation = 360
	})
	_ARG_0_._secondary_inf_glow = _ARG_0_._player_panel:child("weapons_panel"):child("secondary_weapon_panel"):bitmap({
		name = "glow",
		texture = "guis/textures/pd2/crimenet_marker_glow",
		h = 64,
		w = 64,
		color = tweak_data.hoxhud.infinite_ammo_glow_color,
		blend_mode = "add",
		layer = 2,
		align = "center",
		visible = false,
		rotation = 360
	})
	_ARG_0_._primary_inf_glow:set_center_y(_ARG_0_._player_panel:child("weapons_panel"):child("primary_weapon_panel"):child("ammo_clip"):y() + _ARG_0_._player_panel:child("weapons_panel"):child("primary_weapon_panel"):child("ammo_clip"):h() / 2 - 2)
	_ARG_0_._secondary_inf_glow:set_center_y(_ARG_0_._player_panel:child("weapons_panel"):child("secondary_weapon_panel"):child("ammo_clip"):y() + _ARG_0_._player_panel:child("weapons_panel"):child("secondary_weapon_panel"):child("ammo_clip"):h() / 2 - 2)
	_ARG_0_._primary_inf_glow:set_center_x(_ARG_0_._player_panel:child("weapons_panel"):child("primary_weapon_panel"):child("ammo_clip"):x() + _ARG_0_._player_panel:child("weapons_panel"):child("primary_weapon_panel"):child("ammo_clip"):w() / 2)
	_ARG_0_._secondary_inf_glow:set_center_x(_ARG_0_._player_panel:child("weapons_panel"):child("secondary_weapon_panel"):child("ammo_clip"):x() + _ARG_0_._player_panel:child("weapons_panel"):child("secondary_weapon_panel"):child("ammo_clip"):w() / 2)
end

function HUDTeammate.inject_push_to_talk(_ARG_0_)
	_ARG_0_._panel:bitmap({
		name = "callsign_ptt",
		texture = tweak_data.hud_icons:get_icon_data("pd2_talk")
	})
end

function HUDTeammate.teammate_progress(_ARG_0_, ...)
	_ARG_0_.oldMethods.teammate_progress(_ARG_0_, ...)
	if tweak_data.hoxhud.ANTICHEAT_ONLY then
		return
	end

	if ... then
		_ARG_0_:_start_interact_timer(...)
	else
		_ARG_0_:_stop_interact_timer()
	end
end

function HUDTeammate.update_armor_regen(_ARG_0_, _ARG_1_)
	if _ARG_1_ and _ARG_1_ > 0 and _ARG_0_._armor_regen then
		_ARG_1_ = string.format("%.1f", _ARG_1_) .. "s"
		_ARG_0_._armor_regen:set_text(_ARG_1_)
		_ARG_0_._armor_regen:set_visible(true)
		for _FORV_5_ = 1, 4 do
			_ARG_0_["_armor_regenbg" .. _FORV_5_]:set_text(_ARG_1_)
			_ARG_0_["_armor_regenbg" .. _FORV_5_]:set_visible(true)
		end

	elseif _ARG_0_._armor_regen and _ARG_0_._armor_regen:visible() then
		_ARG_0_._armor_regen:set_visible(false)
		for _FORV_5_ = 1, 4 do
			_ARG_0_["_armor_regenbg" .. _FORV_5_]:set_visible(false)
		end
	end
end

function HUDTeammate._start_interact_timer(_ARG_0_, _ARG_1_)
	_ARG_0_._timer_paused = 0
	_ARG_0_._timer = _ARG_1_
	_ARG_0_._panel:child("condition_timer"):set_font_size(tweak_data.hud_players.timer_size)
	_ARG_0_._panel:child("condition_timer"):set_color(Color.white)
	_ARG_0_._panel:child("condition_timer"):stop()
	_ARG_0_._panel:child("condition_timer"):set_visible(true)
	_ARG_0_._panel:child("condition_timer"):animate(callback(_ARG_0_, _ARG_0_, "_animate_interact_timer"), _ARG_0_._panel:child("condition_timer"))
end

function HUDTeammate._stop_interact_timer(_ARG_0_, _ARG_1_)
	if not alive(_ARG_0_._panel) then
		return
	end

	_ARG_0_._panel:child("condition_timer"):set_visible(false)
	_ARG_0_._panel:child("condition_timer"):stop()
end

function HUDTeammate._animate_interact_timer(_ARG_0_, _ARG_1_, _ARG_2_)
	while _ARG_0_._timer >= 0 do
		if _ARG_0_._timer_paused == 0 then
			_ARG_2_:set_text(string.format("%.1f", 0 < _ARG_0_._timer - coroutine.yield() and _ARG_0_._timer - coroutine.yield() or 0) .. "s")
			_ARG_2_:set_color(Color(_ARG_0_._timer / _ARG_0_._timer, 1, _ARG_0_._timer / _ARG_0_._timer))
		end
	end
end

function HUDTeammate.set_interact_text(_ARG_0_, _ARG_1_)
	if not _ARG_0_._interact_info then
		return
	end

	_ARG_0_._interact_info:set_text(_ARG_1_)
	_ARG_0_._interact_info_bg:set_w(_ARG_0_._interact_info:text_rect() + 8)
	_ARG_0_._interact_info_bg:set_right(_ARG_0_._interact_info:right() + 4)
end

function HUDTeammate.set_interact_visible(_ARG_0_, _ARG_1_)
	if _ARG_0_._interact_info_panel and not tweak_data.hoxhud.ANTICHEAT_ONLY then
		_ARG_0_._interact_info_panel:set_visible(_ARG_1_)
	end
end

function HUDTeammate.check_and_truncate_name(_ARG_0_)
	while _ARG_0_._kill_counter:x() + _ARG_0_._kill_counter:text_rect() + 2 > _ARG_0_._kill_counter:parent():w() do
		if _ARG_0_._panel:child("name"):font_size() > 15.3 then
			_ARG_0_._panel:child("name"):set_font_size(_ARG_0_._panel:child("name"):font_size() - 0.1)
		else
			_ARG_0_._panel:child("name"):set_text(_ARG_0_._panel:child("name"):text():sub(1, _ARG_0_._panel:child("name"):text():len() - 1) .. "...")
		end

		_ARG_0_._panel:child("name_bg"):set_w(_ARG_0_._panel:child("name"):text_rect() + 2)
		_ARG_0_._panel:child("name"):set_y(_ARG_0_._panel:child("name_bg"):y())
		_ARG_0_._panel:child("kill_icon"):set_left(_ARG_0_._panel:child("name"):left() + _ARG_0_._panel:child("name"):text_rect())
		_ARG_0_._kill_counter:set_left(_ARG_0_._panel:child("kill_icon"):right())
		_ARG_0_._kill_counter_bg:set_w(_ARG_0_._kill_counter:text_rect() + 4)
		_ARG_0_._kill_counter_bg:set_left(_ARG_0_._kill_counter:left() - 2)
	end

	_ARG_0_._kill_counter_bg:set_w(_ARG_0_._kill_counter:text_rect() + 4)
end

function HUDTeammate.set_name(_ARG_0_, _ARG_1_, _ARG_2_)
	if tweak_data.hoxhud.ANTICHEAT_ONLY or tweak_data.hoxhud.disable_kill_counter then
		return _ARG_0_.oldMethods.set_name(_ARG_0_, _ARG_1_)
	end

	if not _ARG_0_._main_player then
		_ARG_1_ = managers.hud:full_name_for_player(_ARG_0_._peer_id, _ARG_1_)
	else
		_ARG_0_._kill_counter:set_text(tweak_data.hoxhud:format_kills(0, 0, 0, 0, managers.statistics:get_session_killed()))
	end

	_ARG_0_._panel:child("kill_icon"):set_leftbottom(_ARG_0_._panel:child("name"):right() + 3, _ARG_0_._panel:child("name"):bottom())
	_ARG_0_._kill_counter:set_leftbottom(_ARG_0_._panel:child("kill_icon"):right(), _ARG_0_._panel:child("name"):bottom())
	if not _ARG_0_._main_player then
		_ARG_0_._panel:child("kill_icon"):set_bottom(_ARG_0_._panel:h() - 30)
		_ARG_0_._kill_counter:set_bottom(_ARG_0_._panel:h() - 30)
	end

	_ARG_0_._kill_counter_bg:set_w(_ARG_0_._kill_counter:text_rect() + 4)
	_ARG_0_._kill_counter_bg:set_leftbottom(_ARG_0_._kill_counter:left() - 2, _ARG_0_._kill_counter:bottom() - 1)
	_ARG_0_._panel:child("name"):set_vertical("center")
	_ARG_0_:check_and_truncate_name()
	return (_ARG_0_.oldMethods.set_name(_ARG_0_, _ARG_1_))
end

function HUDTeammate.set_ai(_ARG_0_, _ARG_1_)
	_ARG_0_.oldMethods.set_ai(_ARG_0_, _ARG_1_)
	if tweak_data.hoxhud.ANTICHEAT_ONLY or tweak_data.hoxhud.disable_kill_counter then
		return
	end

	_ARG_0_._kill_counter:set_visible(not _ARG_1_)
	_ARG_0_._kill_counter_bg:set_visible(not _ARG_1_)
	_ARG_0_._panel:child("kill_icon"):set_visible(not _ARG_1_)
end

function HUDTeammate.update_kill_counter(_ARG_0_, ...)
	if tweak_data.hoxhud.ANTICHEAT_ONLY or tweak_data.hoxhud.disable_kill_counter then
		return
	end

	_ARG_0_._kill_counter:set_text(_ARG_0_._main_player and tweak_data.hoxhud:format_kills(...) or tostring(...))
	_ARG_0_:check_and_truncate_name()
end

function HUDTeammate.increment_kill_counter(_ARG_0_)
	_ARG_0_._kills = _ARG_0_._kills + 1
	_ARG_0_:update_kill_counter(_ARG_0_._kills)
end

function HUDTeammate.set_has_infinite_ammo(_ARG_0_, _ARG_1_)
	if _ARG_1_ == _ARG_0_._ammo_is_infinite or tweak_data.hoxhud.ANTICHEAT_ONLY then
		return
	end

	_ARG_0_._primary_inf_glow:set_visible(_ARG_1_)
	_ARG_0_._secondary_inf_glow:set_visible(_ARG_1_)
	_ARG_0_._primary_inf_glow:stop()
	_ARG_0_._ammo_is_infinite = _ARG_1_
	if _ARG_1_ then
		_ARG_0_._last_primary_ammo = _ARG_0_._player_panel:child("weapons_panel"):child("primary_weapon_panel"):child("ammo_clip"):text()
		_ARG_0_._last_secondary_ammo = _ARG_0_._player_panel:child("weapons_panel"):child("secondary_weapon_panel"):child("ammo_clip"):text()
		_ARG_0_._player_panel:child("weapons_panel"):child("primary_weapon_panel"):child("ammo_clip"):set_text("8")
		_ARG_0_._player_panel:child("weapons_panel"):child("primary_weapon_panel"):child("ammo_clip"):set_rotation(90)
		_ARG_0_._player_panel:child("weapons_panel"):child("primary_weapon_panel"):child("ammo_clip"):set_y(_ARG_0_._player_panel:child("weapons_panel"):child("primary_weapon_panel"):child("ammo_clip"):y() - 4)
		_ARG_0_._player_panel:child("weapons_panel"):child("secondary_weapon_panel"):child("ammo_clip"):set_text("8")
		_ARG_0_._player_panel:child("weapons_panel"):child("secondary_weapon_panel"):child("ammo_clip"):set_rotation(90)
		_ARG_0_._player_panel:child("weapons_panel"):child("secondary_weapon_panel"):child("ammo_clip"):set_y(_ARG_0_._player_panel:child("weapons_panel"):child("secondary_weapon_panel"):child("ammo_clip"):y() - 4)
		_ARG_0_._primary_inf_glow:animate(callback(_ARG_0_, _ARG_0_, "_animate_glow"), {
			_ARG_0_._primary_inf_glow,
			_ARG_0_._secondary_inf_glow
		}, "_ammo_is_infinite", tweak_data.hoxhud.infinite_ammo_flash_speed)
	else
		_ARG_0_._player_panel:child("weapons_panel"):child("primary_weapon_panel"):child("ammo_clip"):set_text(_ARG_0_._last_primary_ammo or "000")
		_ARG_0_._player_panel:child("weapons_panel"):child("primary_weapon_panel"):child("ammo_clip"):set_rotation(0)
		_ARG_0_._player_panel:child("weapons_panel"):child("primary_weapon_panel"):child("ammo_clip"):set_y(_ARG_0_._player_panel:child("weapons_panel"):child("primary_weapon_panel"):child("ammo_clip"):y() + 4)
		_ARG_0_._player_panel:child("weapons_panel"):child("secondary_weapon_panel"):child("ammo_clip"):set_text(_ARG_0_._last_secondary_ammo or "000")
		_ARG_0_._player_panel:child("weapons_panel"):child("secondary_weapon_panel"):child("ammo_clip"):set_rotation(0)
		_ARG_0_._player_panel:child("weapons_panel"):child("secondary_weapon_panel"):child("ammo_clip"):set_y(_ARG_0_._player_panel:child("weapons_panel"):child("secondary_weapon_panel"):child("ammo_clip"):y() + 4)
	end
end

function HUDTeammate.set_ammo_amount_by_type(_ARG_0_, ...)
	if _ARG_0_._ammo_is_infinite then
		return
	end

	_ARG_0_.oldMethods.set_ammo_amount_by_type(_ARG_0_, ...)
end

function HUDTeammate.set_has_damage_dampener(_ARG_0_, _ARG_1_)
	if _ARG_1_ == _ARG_0_._dampener_glow_enabled or tweak_data.hoxhud.ANTICHEAT_ONLY then
		return
	end

	_ARG_0_._radial_health_glow:set_visible(_ARG_1_)
	_ARG_0_._radial_health_glow:stop()
	_ARG_0_._dampener_glow_enabled = _ARG_1_
	if _ARG_1_ then
		_ARG_0_._radial_health_glow:animate(callback(_ARG_0_, _ARG_0_, "_animate_glow"), {
			_ARG_0_._radial_health_glow
		}, "_dampener_glow_enabled", tweak_data.hoxhud.health_dampen_flash_speed)
	end
end

function HUDTeammate._animate_glow(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_)
	while _ARG_0_[_ARG_3_] do
		for _FORV_11_, _FORV_12_ in ipairs(_ARG_2_) do
			_FORV_12_:set_alpha((math.abs(math.sin(((_ARG_4_ or 4) + coroutine.yield()) * 360 * (_ARG_4_ or 4) / 4))))
		end
	end
end

function HUDTeammate.init(_ARG_0_, ...)
	_ARG_0_.oldMethods.init(_ARG_0_, ...)
	if tweak_data.hoxhud.ANTICHEAT_ONLY then
		return
	end

	if _ARG_0_._main_player then
		_ARG_0_:inject_stamina()
		_ARG_0_:inject_infinite_ammo_glow()
		_ARG_0_:inject_health_glow()
		_ARG_0_:inject_armor_regen_timer()
	else
		_ARG_0_:inject_interact_info()
	end

	if not tweak_data.hoxhud.disable_kill_counter then
		_ARG_0_:inject_killcounter()
	end

	_ARG_0_:inject_revivecounter()
	_ARG_0_:inject_push_to_talk()
end

