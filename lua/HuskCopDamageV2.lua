clone_methods(HuskCopDamage)
function HuskCopDamage.die(_ARG_0_, ...)
	if managers.groupai:state():is_enemy_converted_to_criminal(_ARG_0_._unit) then
		managers.groupai:state()._converted_police[_ARG_0_._unit:key()] = nil
		managers.hud:set_control_info({
			nr_jokered = managers.groupai:state():get_amount_enemies_converted_to_criminals() or 0
		})
	end

	_ARG_0_.oldMethods.die(_ARG_0_, ...)
end

