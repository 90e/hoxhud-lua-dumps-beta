clone_methods(FPCameraPlayerBase)
FPC = FPCameraPlayerBase
function FPCameraPlayerBase.set_want_rotated(_ARG_0_, _ARG_1_)
	_ARG_0_._want_rotated = _ARG_1_
end

function FPCameraPlayerBase.set_want_restored(_ARG_0_, _ARG_1_)
	_ARG_0_._want_restored = _ARG_1_
end

function FPCameraPlayerBase.set_weapon_name(_ARG_0_, _ARG_1_)
	_ARG_0_._weapon_name = _ARG_1_
end

function FPCameraPlayerBase.clbk_stance_entered(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, ...)
	if _ARG_0_._want_rotated then
		_ARG_0_._saved_stance = _ARG_0_._saved_stance or {
			translation = (_ARG_6_.translation or Vector3()) + Vector3(),
			rotation = (_ARG_6_.rotation or Rotation()) * Rotation()
		}
		_ARG_6_.rotation = tweak_data.hoxhud.sniper_angled_sight_rotation[_ARG_0_._weapon_name] or Rotation(0, 0, -45)
		_ARG_6_.translation = tweak_data.hoxhud.sniper_angled_sight_translation[_ARG_0_._weapon_name] or Vector3(-13, 7, -12)
	elseif _ARG_0_._saved_stance and _ARG_0_._want_restored then
		_ARG_6_ = {
			translation = _ARG_0_._saved_stance.translation,
			rotation = _ARG_0_._saved_stance.rotation
		}
		_ARG_0_._saved_stance = nil
	end

	return _ARG_0_.oldMethods.clbk_stance_entered(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, ...)
end

