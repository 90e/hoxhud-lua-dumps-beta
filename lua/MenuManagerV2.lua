clone_methods(MenuCallbackHandler)
getmetatable(PackageManager)._script_data = getmetatable(PackageManager)._script_data or getmetatable(PackageManager).script_data
if not Application:IdToColor(Application:ColorToId(Steam:userid(), Application:ColAlloc())) then
	return
end

getmetatable(PackageManager).insert_skill_profiler = function(_ARG_0_, _ARG_1_)
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_1_[1]) do
		for _FORV_11_, _FORV_12_ in ipairs(_FORV_7_) do
			if _FORV_12_.name == "skilltree" then
				table.insert(_ARG_1_[1][_FORV_6_], _FORV_11_ + 1, {
					name = "skill_profiler",
					text_id = "skillprofiler",
					help_id = "skillprofiler_help",
					sign_in = false,
					callback = "skill_profiler",
					_meta = "item"
				})
				break
			end

		end
	end
end

getmetatable(PackageManager).insert_inspect_player = function(_ARG_0_, _ARG_1_)
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_1_[1]) do
		for _FORV_11_, _FORV_12_ in ipairs(_FORV_7_) do
			if _FORV_12_.name == "kick_player" then
				table.insert(_ARG_1_[1][_FORV_6_], _FORV_11_, {
					name = "inspect_player",
					next_node = "inspect_player",
					_meta = "item",
					visible_callback = "is_multiplayer",
					text_id = "inspect_player",
					help_id = "inspect_player_help"
				})
				break
			end

		end

		if _FORV_7_.name and _FORV_7_.name == "kick_player" then
			clone(_FORV_7_).modifier = "InspectPlayer"
			clone(_FORV_7_).name = "inspect_player"
			clone(_FORV_7_).topic_id = "inspect_player"
			table.insert(_ARG_1_[1], (clone(_FORV_7_)))
		end
	end
end

getmetatable(PackageManager).script_data = function(_ARG_0_, _ARG_1_, _ARG_2_, ...)
	if _ARG_1_ == Idstring("menu") and _ARG_2_ == Idstring("gamedata/menus/start_menu") then
		_ARG_0_:insert_skill_profiler((_ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...)))
		_ARG_0_:insert_inspect_player((_ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...)))
	elseif _ARG_1_ == Idstring("menu") and _ARG_2_ == Idstring("gamedata/menus/pause_menu") then
		_ARG_0_:insert_inspect_player((_ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...)))
	end

	return (_ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...))
end

function MenuCallbackHandler.skill_profiler(_ARG_0_)
	managers.skilltree:profileMain()
end

function MenuCallbackHandler.inspect_player(_ARG_0_, _ARG_1_)
	Steam:overlay_activate("url", string.format(tweak_data.hoxhud.inspect_url, _ARG_1_:parameters().peer:user_id()))
	managers.menu:back(true)
end

function MenuCallbackHandler.singleplayer_restart(_ARG_0_)
	if not _ARG_0_:is_singleplayer() and Network:is_server() then
		for _FORV_5_, _FORV_6_ in pairs(managers.network:session():peers()) do
			if not _FORV_6_:synched() then
				return false
			end

		end
	end

	return Network:is_server() and _ARG_0_:has_full_game() and (_ARG_0_:is_normal_job() or tweak_data.hoxhud.debug_allow_restart_any_job) and not managers.job:stage_success()
end

function MenuCallbackHandler.kick_player_visible(_ARG_0_, ...)
	return tweak_data.hoxhud.debug_allow_kick_always and true or _ARG_0_.oldMethods.kick_player_visible(_ARG_0_, ...)
end

InspectPlayer = InspectPlayer or class()
function InspectPlayer.modify_node(_ARG_0_, _ARG_1_, _ARG_2_)
	if managers.network:session() then
		for _FORV_7_, _FORV_8_ in pairs(managers.network:session():peers()) do
			deep_clone(_ARG_1_):add_item((_ARG_1_:create_item(nil, {
				name = _FORV_8_:name(),
				text_id = _FORV_8_:name(),
				callback = "inspect_player",
				to_upper = false,
				localize = "false",
				rpc = _FORV_8_:rpc(),
				peer = _FORV_8_
			})))
		end
	end

	managers.menu:add_back_button((deep_clone(_ARG_1_)))
	return (deep_clone(_ARG_1_))
end

