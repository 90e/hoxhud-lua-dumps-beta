clone_methods(WeaponLaser)
function WeaponLaser.init(_ARG_0_, ...)
	_ARG_0_.oldMethods.init(_ARG_0_, ...)
	if tweak_data.hoxhud.weapon_laser_color then
		if tweak_data.hoxhud.weapon_laser_color == "fabulousputin" then
			_ARG_0_.update = _ARG_0_.fabulousputin
		else
			_ARG_0_._themes.hoxhud = tweak_data.hoxhud.weapon_laser_color
			_ARG_0_:set_color_by_theme("hoxhud")
		end
	end
end

function WeaponLaser.fabulousputin(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	_ARG_0_.oldMethods.update(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	_ARG_0_._themes.hoxhud = {
		light = Color(math.sin(135 * _ARG_2_ + 0) / 2 + 0.5, math.sin(140 * _ARG_2_ + 60) / 2 + 0.5, math.sin(145 * _ARG_2_ + 120) / 2 + 0.5) * 10,
		glow = Color(math.sin(135 * _ARG_2_ + 0) / 2 + 0.5, math.sin(140 * _ARG_2_ + 60) / 2 + 0.5, math.sin(145 * _ARG_2_ + 120) / 2 + 0.5) / 2,
		brush = Color(0.5, math.sin(135 * _ARG_2_ + 0) / 2 + 0.5, math.sin(140 * _ARG_2_ + 60) / 2 + 0.5, math.sin(145 * _ARG_2_ + 120) / 2 + 0.5)
	}
	_ARG_0_:set_color_by_theme("hoxhud")
end

