clone_methods(CopDamage)
function CopDamage.init(_ARG_0_, ...)
	_ARG_0_.oldMethods.init(_ARG_0_, ...)
	_ARG_0_._hud = HUDCopDamage:new(_ARG_0_._unit)
end

function CopDamage.damage_bullet(_ARG_0_, _ARG_1_, ...)
	if _ARG_0_._dead or _ARG_0_._invulnerable then
		return
	end

	if _ARG_1_.damage > 0.09 and _ARG_1_.attacker_unit == managers.player:player_unit() then
		_ARG_0_._hud:show_damage(_ARG_1_.damage, _ARG_0_._dead, _ARG_0_._head_body_name and _ARG_1_.col_ray.body and _ARG_1_.col_ray.body:name() == _ARG_0_._ids_head_body_name)
	end

	return unpack({
		_ARG_0_.oldMethods.damage_bullet(_ARG_0_, _ARG_1_, ...)
	})
end

function CopDamage.damage_explosion(_ARG_0_, _ARG_1_, ...)
	if _ARG_0_._dead or _ARG_0_._invulnerable then
		return
	end

	if _ARG_1_.damage > 0.09 and _ARG_1_.attacker_unit and _ARG_1_.attacker_unit == managers.player:player_unit() then
		_ARG_0_._hud:show_damage(_ARG_1_.damage, _ARG_0_._dead, false)
	end

	return unpack({
		_ARG_0_.oldMethods.damage_explosion(_ARG_0_, _ARG_1_, ...)
	})
end

function CopDamage.damage_melee(_ARG_0_, _ARG_1_, ...)
	if _ARG_0_._dead or _ARG_0_._invulnerable then
		return
	end

	if _ARG_1_.damage > 0.09 and _ARG_1_.attacker_unit == managers.player:player_unit() then
		_ARG_0_._hud:show_damage(_ARG_1_.damage, _ARG_0_._dead, _ARG_0_._head_body_name and _ARG_1_.col_ray.body and _ARG_1_.col_ray.body:name() == _ARG_0_._ids_head_body_name)
	end

	return unpack({
		_ARG_0_.oldMethods.damage_melee(_ARG_0_, _ARG_1_, ...)
	})
end

