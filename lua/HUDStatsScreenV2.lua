clone_methods(HUDStatsScreen)
HUDStatsScreen._exceptions = {}
function HUDStatsScreen.init(_ARG_0_)
	_ARG_0_.oldMethods.init(_ARG_0_)
	_ARG_0_._hud_heist_timer = HUDHeistTimer:new({
		panel = _ARG_0_._full_hud_panel:child("left_panel"):panel({
			visible = true,
			layer = 1,
			name = "heist_time_panel",
			x = 10,
			y = _ARG_0_._full_hud_panel:child("left_panel"):h() - 60,
			h = 40,
			w = _ARG_0_._full_hud_panel:child("left_panel"):w() - 10
		})
	})
	if not tweak_data.hoxhud.ANTICHEAT_ONLY then
		_ARG_0_:destroy_useless_info((_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel")))
		if managers.job:is_current_job_professional() then
			managers.hud:make_fine_text((_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"):text({
				layer = 0,
				name = "pro_job",
				color = tweak_data.screen_colors.pro_color,
				font_size = tweak_data.hud_stats.objectives_title_size,
				font = tweak_data.hud_stats.objectives_font,
				text = managers.localization:to_upper_text("cn_menu_pro_job"),
				align = "left",
				vertical = "top",
				w = 512,
				h = _ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"):child("day_title"):h()
			})))
			_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"):text({
				layer = 0,
				name = "pro_job",
				color = tweak_data.screen_colors.pro_color,
				font_size = tweak_data.hud_stats.objectives_title_size,
				font = tweak_data.hud_stats.objectives_font,
				text = managers.localization:to_upper_text("cn_menu_pro_job"),
				align = "left",
				vertical = "top",
				w = 512,
				h = _ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"):child("day_title"):h()
			}):set_w(_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"):w())
			_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"):text({
				layer = 0,
				name = "pro_job",
				color = tweak_data.screen_colors.pro_color,
				font_size = tweak_data.hud_stats.objectives_title_size,
				font = tweak_data.hud_stats.objectives_font,
				text = managers.localization:to_upper_text("cn_menu_pro_job"),
				align = "left",
				vertical = "top",
				w = 512,
				h = _ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"):child("day_title"):h()
			}):set_top(_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"):child("day_title"):top())
			_ARG_0_._exceptions.pro_job = true
		end

		_ARG_0_._timer_panel = HUDTimerManager:new({
			panel = _ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel")
		}, _ARG_0_:create_text_with_counter(_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"), "melee_kills", _ARG_0_:create_text_with_counter(_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"), "tasers_killed", _ARG_0_:create_text_with_counter(_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"), "snipers_killed", _ARG_0_:create_text_with_counter(_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"), "shields_killed", _ARG_0_:create_text_with_counter(_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"), "spoocs_killed", _ARG_0_:create_text_with_counter(_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"), "tanks_killed", _ARG_0_:create_text_with_counter(_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"), "accuracy", _ARG_0_:create_text_with_counter(_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"), "spending_cash", _ARG_0_:create_text_with_counter(_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"), "cleaner_costs", _ARG_0_:create_text_with_counter(_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"), "offshore_cash", _ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"):child("day_payout"), managers.localization:to_upper_text("hud_offshore_account") .. ": ", tweak_data.hud_stats.loot_size, tweak_data.screen_colors.text, tweak_data.screen_colors.text), managers.localization:to_upper_text("hud_cleaner_costs") .. ": ", tweak_data.hud_stats.loot_size, tweak_data.screen_colors.text, tweak_data.screen_colors.text), managers.localization:to_upper_text("hud_spending_cash") .. ": ", tweak_data.hud_stats.loot_size, tweak_data.screen_colors.text, tweak_data.screen_colors.friend_color):bottom() + _ARG_0_:create_text_with_counter(_ARG_0_._full_hud_panel:child("right_panel"), "stats", _ARG_0_:create_text_with_counter(_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"), "spending_cash", _ARG_0_:create_text_with_counter(_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"), "cleaner_costs", _ARG_0_:create_text_with_counter(_ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"), "offshore_cash", _ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"):child("day_payout"), managers.localization:to_upper_text("hud_offshore_account") .. ": ", tweak_data.hud_stats.loot_size, tweak_data.screen_colors.text, tweak_data.screen_colors.text), managers.localization:to_upper_text("hud_cleaner_costs") .. ": ", tweak_data.hud_stats.loot_size, tweak_data.screen_colors.text, tweak_data.screen_colors.text), managers.localization:to_upper_text("hud_spending_cash") .. ": ", tweak_data.hud_stats.loot_size, tweak_data.screen_colors.text, tweak_data.screen_colors.friend_color):bottom() + _ARG_0_._full_hud_panel:child("right_panel"):child("day_wrapper_panel"):y() + 20, "STATS", tweak_data.hud_stats.loot_size, tweak_data.screen_colors.text, nil, 20):text_rect(), managers.localization:text("menu_stats_hit_accuracy") .. " ", tweak_data.hud_stats.loot_size, tweak_data.screen_colors.text, tweak_data.screen_colors.risk), managers.localization:to_upper_text("hud_tanks_killed_text"), tweak_data.hud_stats.day_description_size, tweak_data.screen_colors.pro_color, Color.white), managers.localization:to_upper_text("hud_spoocs_killed_text"), tweak_data.hud_stats.day_description_size, tweak_data.screen_colors.friend_color, Color.white), managers.localization:to_upper_text("hud_shields_killed_text"), tweak_data.hud_stats.day_description_size, Color("696969"), Color.white), managers.localization:to_upper_text("hud_snipers_killed_text"), tweak_data.hud_stats.day_description_size, Color("E98C2D"), Color.white), managers.localization:to_upper_text("hud_tasers_killed_text"), tweak_data.hud_stats.day_description_size, Color("008BDB"), Color.white), managers.localization:to_upper_text("hud_melee_kills_text"), tweak_data.hud_stats.day_description_size, Color("944500"), Color.white):bottom() + 20)
	end

	_ARG_0_._right_panel = _ARG_0_._full_hud_panel:child("right_panel")
end

function HUDStatsScreen.create_text_with_counter(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, _ARG_4_, _ARG_5_, _ARG_6_, _ARG_7_, _ARG_8_)
	managers.hud:make_fine_text((_ARG_1_:text({
		name = _ARG_2_ .. "_title",
		text = _ARG_4_,
		font = tweak_data.menu.pd2_medium_font,
		font_size = _ARG_5_,
		color = _ARG_6_
	})))
	_ARG_1_:text({
		name = _ARG_2_ .. "_title",
		text = _ARG_4_,
		font = tweak_data.menu.pd2_medium_font,
		font_size = _ARG_5_,
		color = _ARG_6_
	}):set_w(_ARG_1_:w())
	_ARG_1_:text({
		name = _ARG_2_ .. "_title",
		text = _ARG_4_,
		font = tweak_data.menu.pd2_medium_font,
		font_size = _ARG_5_,
		color = _ARG_6_
	}):set_top(math.round(type(_ARG_3_) == "userdata" and _ARG_3_:bottom() or _ARG_3_))
	_ARG_1_:text({
		name = _ARG_2_ .. "_title",
		text = _ARG_4_,
		font = tweak_data.menu.pd2_medium_font,
		font_size = _ARG_5_,
		color = _ARG_6_
	}):set_x(_ARG_1_:text({
		name = _ARG_2_ .. "_title",
		text = _ARG_4_,
		font = tweak_data.menu.pd2_medium_font,
		font_size = _ARG_5_,
		color = _ARG_6_
	}):x() + (_ARG_8_ or 0))
	_ARG_0_._exceptions[_ARG_2_ .. "_title"] = true
	if not _ARG_7_ then
		return (_ARG_1_:text({
			name = _ARG_2_ .. "_title",
			text = _ARG_4_,
			font = tweak_data.menu.pd2_medium_font,
			font_size = _ARG_5_,
			color = _ARG_6_
		}))
	end

	managers.hud:make_fine_text((_ARG_1_:text({
		name = _ARG_2_ .. "_text",
		text = "0",
		font = tweak_data.menu.pd2_medium_font,
		font_size = _ARG_5_,
		color = _ARG_7_
	})))
	_ARG_1_:text({
		name = _ARG_2_ .. "_text",
		text = "0",
		font = tweak_data.menu.pd2_medium_font,
		font_size = _ARG_5_,
		color = _ARG_7_
	}):set_w(_ARG_1_:w() - _ARG_1_:text({
		name = _ARG_2_ .. "_title",
		text = _ARG_4_,
		font = tweak_data.menu.pd2_medium_font,
		font_size = _ARG_5_,
		color = _ARG_6_
	}):text_rect())
	_ARG_1_:text({
		name = _ARG_2_ .. "_text",
		text = "0",
		font = tweak_data.menu.pd2_medium_font,
		font_size = _ARG_5_,
		color = _ARG_7_
	}):set_left(_ARG_1_:text({
		name = _ARG_2_ .. "_title",
		text = _ARG_4_,
		font = tweak_data.menu.pd2_medium_font,
		font_size = _ARG_5_,
		color = _ARG_6_
	}):text_rect() + _ARG_1_:text({
		name = _ARG_2_ .. "_title",
		text = _ARG_4_,
		font = tweak_data.menu.pd2_medium_font,
		font_size = _ARG_5_,
		color = _ARG_6_
	}):x() + (_ARG_8_ or 0))
	_ARG_1_:text({
		name = _ARG_2_ .. "_text",
		text = "0",
		font = tweak_data.menu.pd2_medium_font,
		font_size = _ARG_5_,
		color = _ARG_7_
	}):set_top(math.round(type(_ARG_3_) == "userdata" and _ARG_3_:bottom() or _ARG_3_))
	_ARG_0_._exceptions[_ARG_2_ .. "_text"] = true
	return _ARG_1_:text({
		name = _ARG_2_ .. "_text",
		text = "0",
		font = tweak_data.menu.pd2_medium_font,
		font_size = _ARG_5_,
		color = _ARG_7_
	}), (_ARG_1_:text({
		name = _ARG_2_ .. "_title",
		text = _ARG_4_,
		font = tweak_data.menu.pd2_medium_font,
		font_size = _ARG_5_,
		color = _ARG_6_
	}))
end

function HUDStatsScreen.destroy_useless_info(_ARG_0_, _ARG_1_)
	for _FORV_6_, _FORV_7_ in pairs(_ARG_1_:children()) do
		if (false or _FORV_7_:name() == "bains_plan") and (not _ARG_0_._timer_panel or _FORV_7_:key() ~= _ARG_0_._timer_panel:root_panel():key()) and not _ARG_0_._exceptions[_FORV_7_:name()] then
			_FORV_7_:set_visible(false)
		end
	end
end

function HUDStatsScreen.feed_heist_time(_ARG_0_, _ARG_1_)
	_ARG_0_._hud_heist_timer:set_time(_ARG_1_)
end

function HUDStatsScreen.add_timer(_ARG_0_, ...)
	_ARG_0_._right_panel:set_alpha(1)
	_ARG_0_._right_panel:set_alpha((_ARG_0_._right_panel:alpha()))
	return (_ARG_0_._timer_panel:add_timer(...))
end

function HUDStatsScreen.del_timer(_ARG_0_, ...)
	_ARG_0_._timer_panel:del_timer(...)
end

function HUDStatsScreen.timer_manager(_ARG_0_)
	return _ARG_0_._timer_panel
end

function HUDStatsScreen._update_stats_screen_day(_ARG_0_, _ARG_1_)
	_ARG_0_.oldMethods._update_stats_screen_day(_ARG_0_, _ARG_1_)
	if tweak_data.hoxhud.ANTICHEAT_ONLY then
		return
	end

	_ARG_0_:destroy_useless_info((_ARG_1_:child("day_wrapper_panel")))
	_ARG_1_:child("day_wrapper_panel"):set_visible(true)
	if _ARG_1_:child("day_wrapper_panel"):child("pro_job") then
		_ARG_1_:child("day_wrapper_panel"):child("day_title"):set_text(_ARG_1_:child("day_wrapper_panel"):child("day_title"):text() .. "  ")
		_ARG_1_:child("day_wrapper_panel"):child("pro_job"):set_left(_ARG_1_:child("day_wrapper_panel"):child("day_title"):text_rect())
	end

	_ARG_1_:child("day_wrapper_panel"):child("offshore_cash_text"):set_text("" .. managers.experience:cash_string(managers.money:get_potential_payout_from_current_stage() - math.round(managers.money:get_potential_payout_from_current_stage() * managers.money:get_tweak_value("money_manager", "offshore_rate"))))
	_ARG_1_:child("day_wrapper_panel"):child("cleaner_costs_text"):set_text(managers.experience:cash_string(managers.money:get_civilian_deduction() * (managers.statistics:session_total_civilian_kills() or 0)) .. " (" .. (managers.statistics:session_total_civilian_kills() or 0) .. ")")
	_ARG_1_:child("day_wrapper_panel"):child("spending_cash_text"):set_text(managers.experience:cash_string(math.round(managers.money:get_potential_payout_from_current_stage() * managers.money:get_tweak_value("money_manager", "offshore_rate")) - managers.money:get_civilian_deduction() * (managers.statistics:session_total_civilian_kills() or 0)))
	_ARG_1_:child("day_wrapper_panel"):child("accuracy_text"):set_text(managers.statistics:session_hit_accuracy() .. "%")
	_ARG_1_:child("day_wrapper_panel"):child("tanks_killed_text"):set_text("" .. managers.statistics:get_session_killed().tank.count)
	_ARG_1_:child("day_wrapper_panel"):child("spoocs_killed_text"):set_text("" .. managers.statistics:get_session_killed().spooc.count)
	_ARG_1_:child("day_wrapper_panel"):child("shields_killed_text"):set_text("" .. managers.statistics:get_session_killed().shield.count)
	_ARG_1_:child("day_wrapper_panel"):child("snipers_killed_text"):set_text("" .. managers.statistics:get_session_killed().sniper.count)
	_ARG_1_:child("day_wrapper_panel"):child("tasers_killed_text"):set_text("" .. managers.statistics:get_session_killed().taser.count)
	_ARG_1_:child("day_wrapper_panel"):child("melee_kills_text"):set_text("" .. managers.statistics:get_session_melee_kills())
	if 0 <= math.round(managers.money:get_potential_payout_from_current_stage() * managers.money:get_tweak_value("money_manager", "offshore_rate")) - managers.money:get_civilian_deduction() * (managers.statistics:session_total_civilian_kills() or 0) then
		_ARG_1_:child("day_wrapper_panel"):child("spending_cash_text"):set_color(tweak_data.screen_colors.friend_color)
	else
		_ARG_1_:child("day_wrapper_panel"):child("spending_cash_text"):set_color(tweak_data.screen_colors.heat_cold_color)
	end
end

