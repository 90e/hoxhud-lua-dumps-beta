clone_methods(TweakData)
function TweakData.init(_ARG_0_, ...)
	TweakData.oldMethods.init(_ARG_0_, ...)
	_ARG_0_.hoxhud = HoxHudTweakData:new()
	for _FORV_6_, _FORV_7_ in pairs(_ARG_0_.hoxhud.local_strings) do
	end

	for _FORV_6_, _FORV_7_ in pairs({
		[tostring(Idstring(_FORV_6_))] = _FORV_7_
	}) do
		_ARG_0_.hoxhud.local_strings[_FORV_6_] = _FORV_7_
	end
end

tweak_data = TweakData:new()
