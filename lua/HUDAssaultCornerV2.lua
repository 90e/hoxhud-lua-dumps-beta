clone_methods(HUDAssaultCorner)
HUDAssaultCorner._info_boxes = {}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.pagers_info_box.order] = {
	name = "pagers",
	rect = {
		64,
		256,
		64,
		64
	},
	want = "_nr_pagers",
	tweak_data = tweak_data.hoxhud.pagers_info_box
}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.bodybags_info_box.order] = {
	name = "bodybags",
	rect = {
		448,
		128,
		64,
		64
	},
	want = "_nr_bodybags",
	tweak_data = tweak_data.hoxhud.bodybags_info_box
}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.dom_info_box.order] = {
	name = "dominated",
	rect = {
		128,
		512,
		64,
		64
	},
	want = "_nr_dominated",
	tweak_data = tweak_data.hoxhud.dom_info_box
}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.jokers_info_box.order] = {
	name = "jokers",
	rect = {
		384,
		512,
		64,
		64
	},
	want = "_nr_jokered",
	tweak_data = tweak_data.hoxhud.jokers_info_box
}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.sentry_info_box.order] = {
	name = "sentries",
	rect = {
		448,
		320,
		64,
		64
	},
	want = "_nr_sentries",
	tweak_data = tweak_data.hoxhud.sentry_info_box
}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.feedback_info_box.order] = {
	name = "feedback",
	rect = {
		384,
		128,
		64,
		64
	},
	want = "_nr_feedback",
	tweak_data = tweak_data.hoxhud.feedback_info_box
}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.alertedcivs_info_box.order] = {
	name = "alertedcivs",
	rect = {
		384,
		448,
		64,
		64
	},
	want = "_nr_alertedcivs",
	tweak_data = tweak_data.hoxhud.alertedcivs_info_box
}
HUDAssaultCorner._info_boxes[tweak_data.hoxhud.gagemodpack_info_box.order] = {
	name = "gagemodpack",
	rect = {
		0,
		512,
		64,
		64
	},
	want = "_nr_gagepacks",
	tweak_data = tweak_data.hoxhud.gagemodpack_info_box
}
HUDAssaultCorner._info_box_padding = 7
HUDAssaultCorner._icon_size = 38
function HUDAssaultCorner.InjectImprovedInfoBoxes(_ARG_0_)
	_ARG_0_._hud_panel:child("hostages_panel"):set_h(_ARG_0_._hud_panel:child("hostages_panel"):h() + 150)
	for _FORV_5_, _FORV_6_ in ipairs(_ARG_0_._info_box_panels) do
		HUDBGBox_create(_ARG_0_._hud_panel:child("hostages_panel"), {
			w = 38,
			h = 38,
			x = 0,
			y = _ARG_0_._info_box_panels[_FORV_5_ - 1].bg_box:bottom() + _ARG_0_._info_box_padding
		}, {}):set_right(_ARG_0_._hud_panel:child("hostages_panel"):w())
		_ARG_0_._hud_panel:child("hostages_panel"):bitmap({
			name = _FORV_6_.name .. "_icon",
			texture = "guis/textures/pd2/skilltree/icons_atlas",
			valign = "top",
			texture_rect = _FORV_6_.rect,
			color = _FORV_6_.color or Color.white,
			layer = 1,
			x = 0,
			y = _ARG_0_._info_box_panels[_FORV_5_ - 1].bg_box:bottom() + _ARG_0_._info_box_padding,
			h = _ARG_0_._icon_size,
			w = _ARG_0_._icon_size
		}):set_right(HUDBGBox_create(_ARG_0_._hud_panel:child("hostages_panel"), {
			w = 38,
			h = 38,
			x = 0,
			y = _ARG_0_._info_box_panels[_FORV_5_ - 1].bg_box:bottom() + _ARG_0_._info_box_padding
		}, {}):left())
		_FORV_6_.icon, _FORV_6_.num_text, _FORV_6_.bg_box = _ARG_0_._hud_panel:child("hostages_panel"):bitmap({
			name = _FORV_6_.name .. "_icon",
			texture = "guis/textures/pd2/skilltree/icons_atlas",
			valign = "top",
			texture_rect = _FORV_6_.rect,
			color = _FORV_6_.color or Color.white,
			layer = 1,
			x = 0,
			y = _ARG_0_._info_box_panels[_FORV_5_ - 1].bg_box:bottom() + _ARG_0_._info_box_padding,
			h = _ARG_0_._icon_size,
			w = _ARG_0_._icon_size
		}), HUDBGBox_create(_ARG_0_._hud_panel:child("hostages_panel"), {
			w = 38,
			h = 38,
			x = 0,
			y = _ARG_0_._info_box_panels[_FORV_5_ - 1].bg_box:bottom() + _ARG_0_._info_box_padding
		}, {}):text({
			name = "num_" .. _FORV_6_.name,
			text = "0",
			valign = "center",
			align = "center",
			vertical = "center",
			w = HUDBGBox_create(_ARG_0_._hud_panel:child("hostages_panel"), {
				w = 38,
				h = 38,
				x = 0,
				y = _ARG_0_._info_box_panels[_FORV_5_ - 1].bg_box:bottom() + _ARG_0_._info_box_padding
			}, {}):w(),
			h = HUDBGBox_create(_ARG_0_._hud_panel:child("hostages_panel"), {
				w = 38,
				h = 38,
				x = 0,
				y = _ARG_0_._info_box_panels[_FORV_5_ - 1].bg_box:bottom() + _ARG_0_._info_box_padding
			}, {}):h(),
			layer = 1,
			x = 0,
			y = 0,
			color = Color.white,
			font = tweak_data.hud_corner.assault_font,
			font_size = tweak_data.hud_corner.numhostages_size
		}), HUDBGBox_create(_ARG_0_._hud_panel:child("hostages_panel"), {
			w = 38,
			h = 38,
			x = 0,
			y = _ARG_0_._info_box_panels[_FORV_5_ - 1].bg_box:bottom() + _ARG_0_._info_box_padding
		}, {})
	end
end

function HUDAssaultCorner.layout_info_boxes(_ARG_0_, _ARG_1_)
	if tweak_data.hoxhud.ANTICHEAT_ONLY then
		return
	end

	for _FORV_6_, _FORV_7_ in ipairs(_ARG_0_._info_box_panels) do
		if _FORV_7_ and not _FORV_7_.tweak_data.hide and (_ARG_1_ == "stealth" and not _FORV_7_.tweak_data.loud_only or _ARG_1_ == "loud" and not _FORV_7_.tweak_data.stealth_only) and (not _FORV_7_.tweak_data.hideAtZero or _FORV_7_.num_text:text() ~= "0") then
			_FORV_7_.bg_box:set_y(_ARG_0_._info_box_panels[0].bg_box:bottom() + _ARG_0_._info_box_padding)
			_FORV_7_.icon:set_y(_ARG_0_._info_box_panels[0].bg_box:bottom() + _ARG_0_._info_box_padding)
			_FORV_7_.bg_box:set_visible(true)
			_FORV_7_.icon:set_visible(true)
		else
			_FORV_7_.bg_box:set_visible(false)
			_FORV_7_.icon:set_visible(false)
		end
	end
end

function HUDAssaultCorner.init(_ARG_0_, ...)
	_ARG_0_.oldMethods.init(_ARG_0_, ...)
	_ARG_0_._info_box_panels = _ARG_0_._info_boxes
	_ARG_0_._info_box_panels[0] = {
		bg_box = _ARG_0_._hostages_bg_box
	}
	_ARG_0_._nr_hostages = 0
	_ARG_0_._nr_dominated = 0
	_ARG_0_._nr_jokered = 0
	_ARG_0_._nr_sentries = 0
	_ARG_0_._nr_feedback = 0
	_ARG_0_._nr_alertedcivs = 0
	_ARG_0_._nr_gagepacks = 0
	_ARG_0_._nr_bodybags = managers.player:total_body_bags()
	_ARG_0_._nr_pagers = #tweak_data.player.alarm_pager.bluff_success_chance - 1
	if tweak_data.hoxhud.ANTICHEAT_ONLY then
		return
	end

	_ARG_0_:InjectImprovedInfoBoxes()
	_ARG_0_:layout_info_boxes(managers.groupai:state():whisper_mode() and "stealth" or "loud")
	_ARG_0_._hud_panel:child("assault_panel"):set_right(_ARG_0_._hud_panel:w() / 2 + 121 + 24)
	_ARG_0_._hud_panel:child("point_of_no_return_panel"):set_right(_ARG_0_._hud_panel:w() / 2 + 121 + 24)
end

function HUDAssaultCorner.get_num_jokers(_ARG_0_)
	return _ARG_0_._nr_jokered
end

function HUDAssaultCorner.get_num_dominated(_ARG_0_)
	return _ARG_0_._nr_dominated
end

function HUDAssaultCorner.get_num_feedback(_ARG_0_)
	return _ARG_0_._nr_feedback
end

function HUDAssaultCorner._compare_and_update_infobox(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_)
	if _ARG_1_:text() == _ARG_3_ then
		return
	end

	_ARG_1_:set_text(_ARG_3_)
	_ARG_2_:stop()
	_ARG_2_:animate(callback(nil, _G, "HUDBGBox_animate_bg_attention"), {})
end

function HUDAssaultCorner.set_control_info(_ARG_0_, _ARG_1_)
	_ARG_0_._nr_hostages = _ARG_1_.nr_hostages or _ARG_0_._nr_hostages
	_ARG_0_._nr_dominated = _ARG_1_.nr_dominated or _ARG_0_._nr_dominated
	_ARG_0_._nr_jokered = _ARG_1_.nr_jokered or _ARG_0_._nr_jokered
	_ARG_0_._nr_bodybags = _ARG_1_.nr_bodybags or _ARG_0_._nr_bodybags
	_ARG_0_._nr_sentries = _ARG_1_.nr_sentries or _ARG_0_._nr_sentries
	_ARG_0_._nr_feedback = _ARG_1_.nr_feedback or _ARG_0_._nr_feedback
	_ARG_0_._nr_alertedcivs = _ARG_1_.nr_alertedcivs or _ARG_0_._nr_alertedcivs
	_ARG_0_._nr_gagepacks = _ARG_1_.nr_gagepacks or _ARG_0_._nr_gagepacks
	_ARG_0_._nr_pagers = _ARG_1_.pager_answered and (_ARG_0_._nr_pagers - 1 >= 0 and _ARG_0_._nr_pagers - 1 or 0) or _ARG_0_._nr_pagers
	if not tweak_data.hoxhud.ANTICHEAT_ONLY then
		for _FORV_6_, _FORV_7_ in ipairs(_ARG_0_._info_box_panels) do
			_ARG_0_:_compare_and_update_infobox(_FORV_7_.num_text, _FORV_7_.bg_box:child("bg"), (tostring(_ARG_0_[_FORV_7_.want])))
		end
	end

	_ARG_0_:_compare_and_update_infobox(_ARG_0_._hostages_bg_box:child("num_hostages"), _ARG_0_._hostages_bg_box:child("bg"), tostring(_ARG_0_._nr_hostages - _ARG_0_._nr_dominated))
	if false or _FORV_7_.tweak_data.hideAtZero and tostring(_ARG_0_[_FORV_7_.want]) ~= _FORV_7_.num_text:text() then
		_ARG_0_:layout_info_boxes(managers.groupai:state():whisper_mode() and "stealth" or "loud")
	end
end

function HUDAssaultCorner.sync_start_assault(_ARG_0_, ...)
	_ARG_0_.oldMethods.sync_start_assault(_ARG_0_, ...)
	_ARG_0_._hud_panel:child("hostages_panel"):set_visible(not tweak_data.hoxhud.ANTICHEAT_ONLY)
end

function HUDAssaultCorner._start_assault(_ARG_0_, _ARG_1_, ...)
	if Network:is_server() and not tweak_data.hoxhud.disable_enhanced_assault_indicator then
		for _FORV_6_, _FORV_7_ in ipairs(_ARG_1_) do
			_ARG_1_[_FORV_6_] = _FORV_7_ == "hud_assault_assault" and "hud_assault_stats" or _FORV_7_
		end
	end

	return _ARG_0_.oldMethods._start_assault(_ARG_0_, _ARG_1_, ...)
end

function HUDAssaultCorner.show_point_of_no_return_timer(_ARG_0_, ...)
	_ARG_0_.oldMethods.show_point_of_no_return_timer(_ARG_0_, ...)
	_ARG_0_._hud_panel:child("hostages_panel"):set_visible(not tweak_data.hoxhud.ANTICHEAT_ONLY)
end

function HUDAssaultCorner.show_casing(_ARG_0_, ...)
	if tweak_data.hoxhud.ANTICHEAT_ONLY then
		return _ARG_0_.oldMethods.show_casing(_ARG_0_, ...)
	end
end

function HUDAssaultCorner.hide_casing(_ARG_0_, ...)
	if tweak_data.hoxhud.ANTICHEAT_ONLY then
		return _ARG_0_.oldMethods.hide_casing(_ARG_0_, ...)
	end
end

