clone_methods(HUDSuspicion)
function HUDSuspicion.init(_ARG_0_, ...)
	_ARG_0_.oldMethods.init(_ARG_0_, ...)
	if tweak_data.hoxhud.disable_numeric_suspicion or tweak_data.hoxhud.ANTICHEAT_ONLY then
		return
	end

	_ARG_0_._suspicion_text_panel = _ARG_0_._suspicion_panel:panel({
		name = "suspicion_text_panel",
		visible = true,
		x = 0,
		y = 0,
		h = _ARG_0_._suspicion_panel:h(),
		w = _ARG_0_._suspicion_panel:w(),
		layer = 1
	})
	_ARG_0_._suspicion_text = _ARG_0_._suspicion_text_panel:text({
		name = "suspicion_text",
		visible = true,
		text = "",
		valign = "center",
		align = "center",
		layer = 2,
		color = Color.white,
		font = tweak_data.menu.pd2_large_font,
		font_size = 28,
		h = 64
	})
	for _FORV_6_ = 1, 4 do
		_ARG_0_["_bgtext" .. _FORV_6_] = _ARG_0_._suspicion_text_panel:text({
			name = "bgtext" .. _FORV_6_,
			visible = true,
			text = "",
			valign = "center",
			align = "center",
			layer = 1,
			color = Color.black:with_alpha(tweak_data.hoxhud.suspicion_bg_alpha or 0),
			font = tweak_data.menu.pd2_large_font,
			font_size = 28,
			h = 64
		})
	end

	_ARG_0_._suspicion_text:set_y((_FOR_.round(_ARG_0_._suspicion_text_panel:h() / 4)))
	_ARG_0_._bgtext1:set_y(_FOR_.round(_ARG_0_._suspicion_text_panel:h() / 4) - 1)
	_ARG_0_._bgtext1:set_x(_ARG_0_._bgtext1:x() - 1)
	_ARG_0_._bgtext2:set_y(_FOR_.round(_ARG_0_._suspicion_text_panel:h() / 4) + 1)
	_ARG_0_._bgtext2:set_x(_ARG_0_._bgtext2:x() + 1)
	_ARG_0_._bgtext3:set_y(_FOR_.round(_ARG_0_._suspicion_text_panel:h() / 4) + 1)
	_ARG_0_._bgtext3:set_x(_ARG_0_._bgtext3:x() - 1)
	_ARG_0_._bgtext4:set_y(_FOR_.round(_ARG_0_._suspicion_text_panel:h() / 4) - 1)
	_ARG_0_._bgtext4:set_x(_ARG_0_._bgtext4:x() + 1)
	_ARG_0_._text_animation = nil
end

function HUDSuspicion._animate_detection_text(_ARG_0_, _ARG_1_)
	while alive(_ARG_1_) do
		if -1 ~= _ARG_0_._suspicion_value then
			_ARG_0_._suspicion_text:set_color(tweak_data.hoxhud.suspicion_text_max_color * (math.round(_ARG_0_._suspicion_value * 100) / 100) + tweak_data.hoxhud.suspicion_text_min_color * (1 - math.round(_ARG_0_._suspicion_value * 100) / 100))
			for _FORV_10_, _FORV_11_ in ipairs(_ARG_1_:children()) do
				_FORV_11_:set_text(math.round(_ARG_0_._suspicion_value * 100) .. "%")
			end

		elseif 0 + coroutine.yield() > 3 then
			_ARG_0_:hide()
		end
	end
end

function HUDSuspicion.animate_eye(_ARG_0_, ...)
	if not tweak_data.hoxhud.disable_original_suspicion_indicator then
		_ARG_0_.oldMethods.animate_eye(_ARG_0_, ...)
	end

	if _ARG_0_._text_animation or tweak_data.hoxhud.disable_numeric_suspicion or tweak_data.hoxhud.ANTICHEAT_ONLY then
		return
	end

	_ARG_0_._text_animation = _ARG_0_._suspicion_text_panel:animate(callback(_ARG_0_, _ARG_0_, "_animate_detection_text"))
end

function HUDSuspicion.hide(_ARG_0_, ...)
	if _ARG_0_._eye_animation then
		_ARG_0_._suspicion_panel:stop()
		_ARG_0_._eye_animation = nil
	end

	_ARG_0_.oldMethods.hide(_ARG_0_, ...)
	if _ARG_0_._text_animation then
		_ARG_0_._suspicion_text_panel:stop()
		_ARG_0_._text_animation = nil
	end
end

